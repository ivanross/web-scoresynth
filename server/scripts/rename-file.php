<?php
$oldname = $_POST['oldname'];
$newname = $_POST['newname'];

$oldpath = "../users/$oldname";
$newpath = "../users/$newname";
if (!file_exists($oldpath)) {
    echo 'file not found';
} elseif (file_exists($newpath)) {
    echo 'file already exists';
} else {
    $res = rename($oldpath,$newpath);
    if (!$res) {
        echo 'error on renaming';
    }

}
