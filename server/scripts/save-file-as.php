<?php
$oldname = $_POST['oldname'];
$newname = $_POST['newname'];

$oldpath = "../users/$oldname";
$newpath = "../users/$newname";
if(!file_exists($oldpath)) {
    echo 'file not found';
} elseif (file_exists($newpath)) {
    echo 'file already exists';
} else {
    $oldfile = fopen($oldpath, 'r');
    $data = fread($oldfile,filesize($oldpath));

    $newfile = fopen($newpath,'w');
    fwrite($newfile,$data);
    fclose($newfile);
    fclose($oldfile);
}
?>