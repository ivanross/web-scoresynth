<?php
$arr = scandir('../users/');
$pnml = array();
$xml = array();
foreach ($arr as $a) {
    if (substr($a,-5) == ".pnml") {
        array_push($pnml,$a);
    } elseif (substr($a,-4) == ".xml")  {
        array_push($xml,$a);
    }
    
}
echo json_encode(
    array(
        "pnml" => $pnml,
        "xml" => $xml
    )
);
?>