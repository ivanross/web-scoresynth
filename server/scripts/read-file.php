<?php
$fileName = $_POST['fileName'];

$path = "../users/$fileName";
if(!file_exists($path)) {
    echo json_encode(['error','file not found']);
} else {
    $res = file_get_contents($path);
    echo json_encode(['content',$res]);
}
?>