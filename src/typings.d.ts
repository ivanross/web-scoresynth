declare var d3: any;
declare var require: any

type Coord = { x: number, y: number }

interface TransformData {
    scale: number,
    x: number,
    y: number
}
interface FixedDivStyle {
    width: number,
    height: number,
    top: number,
    left: number
}

interface ServerFiles {
    pnml: string[],
    xml: string[]
}