import * as React from "react";
import * as ReactDOM from "react-dom";
import '../style/main';
import { Page } from "./Page";

ReactDOM.render(
    <Page />,
    document.getElementById("app")
);


