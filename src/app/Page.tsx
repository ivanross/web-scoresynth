import * as React from 'react'
import { ControlPanel, Canvas, ToolBar, TabBar, NetTreeView, EmptyCanvas, MenuBar, FileTreeView, FileSelector } from '../components/page'
import { GraphicPetriNet, GraphicPlace, GraphicArc, GraphicTransition, PNAction } from '../logic/graphic';
import { PetriNet } from '../logic/petri';
import * as $ from 'jquery'
import * as Util from '../util/string-util'
import * as JSZip from 'jszip'
import * as FileSaver from 'file-saver'
import { Workspace, WorkspaceNet, WorkspaceFile, FileContent } from '../logic/workspace';
import { log } from 'util';


export { Page }

interface State {
    documentWidth: number,
    documentHeight: number,
    controlPanelWidth: number,
    toolBarHeight: number,
    tabBarHeight: number,
    treeViewWidth: number,
    menuBarHeight: number,
    treeViewTitleHeight: number,
    fileTreeViewHeight: number

    workspace: Workspace,

    action: PNAction
}

class Page extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);
        this.state = {
            action: PNAction.Select,

            workspace: new Workspace(),

            controlPanelWidth: 250,
            toolBarHeight: 40,
            tabBarHeight: 40,
            menuBarHeight: 25,
            treeViewWidth: 200,
            treeViewTitleHeight: 30,
            fileTreeViewHeight: ($("body").height() - 25) / 2,

            documentWidth: $("body").width(),
            documentHeight: $("body").height()
        }
        this.updateCurrentWorkspaceNet = this.updateCurrentWorkspaceNet.bind(this)
        this.keyboardKeyDown = this.keyboardKeyDown.bind(this)

        this.updateCurrentGraphicNet = this.updateCurrentGraphicNet.bind(this)
        this.updateWindowDimension = this.updateWindowDimension.bind(this)
        this.setCurrentNet = this.setCurrentNet.bind(this)
        this.setAction = this.setAction.bind(this)
        this.openNet = this.openNet.bind(this)
        this.closeNet = this.closeNet.bind(this)
        this.closeAll = this.closeAll.bind(this)
        this.closeOthers = this.closeOthers.bind(this)
        this.closeSaved = this.closeSaved.bind(this)
        this.createNet = this.createNet.bind(this)
        this.createNetFromXml = this.createNetFromXml.bind(this)
        this.createFileFromContent = this.createFileFromContent.bind(this)
        this.saveNet = this.saveNet.bind(this)
        this.saveAllNets = this.saveAllNets.bind(this)
        this.deleteNet = this.deleteNet.bind(this)
        this.deleteFile = this.deleteFile.bind(this)
        this.renameNet = this.renameNet.bind(this)
        this.renameFile = this.renameFile.bind(this)
        this.saveAs = this.saveAs.bind(this)
        this.thereIsAtLeastOneNetOpen = this.thereIsAtLeastOneNetOpen.bind(this)
        this.onDrop = this.onDrop.bind(this)
        this.downloadNet = this.downloadNet.bind(this)
        this.downloadFile = this.downloadFile.bind(this)
        this.downloadAll = this.downloadAll.bind(this)
    }


    componentDidMount() {
        $(window).bind('resize', this.updateWindowDimension)
        $(document).bind('keydown', this.keyboardKeyDown)

        // LOAD ALL FILES
        const self = this
        let { workspace } = this.state
        workspace.baseDir = "./server/users"
        $.post('./server/scripts/read-file-of-users.php', data => {
            let files: ServerFiles = JSON.parse(data)
            files.pnml.forEach(f => {
                workspace.addNetFromFileName(f)
            })
            files.xml.forEach(f => {
                workspace.addFileFromFileName(f)
            })
            this.setState({ workspace })
        })
    }
    componentWillUnmount() {
        $(window).unbind('resize', this.updateWindowDimension)
        $(document).unbind('keydown', this.keyboardKeyDown)
    }

    updateWindowDimension() {
        this.setState({
            documentWidth: $('body').width(),
            documentHeight: $('body').height(),
            fileTreeViewHeight: ($('body').height() - this.state.menuBarHeight) / 2
        })
    }

    onDrop(e: React.DragEvent<HTMLDivElement>) {
        e.preventDefault()
        e.stopPropagation()
        let files = e.dataTransfer.files
        for (let i = 0; i < files.length; i++) {
            if (!Util.endsWith(files[i].name, '.pnml', '.xml')) continue
            let reader = new FileReader()
            reader.onload = (e: FileReaderProgressEvent) => {
                if (Util.endsWith(files[i].name, '.pnml'))
                    this.createNetFromXml(files[i].name, e.target.result)
                else
                    this.createFileFromContent(files[i].name, e.target.result)
            }
            reader.readAsText(files[i])
        }
    }

    keyboardKeyDown(e: any) {
        // Ctrl/Cmd + S
        if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
            e.preventDefault()
            this.saveNet(this.state.workspace.currentNet)
        }
    }

    openNet(net: WorkspaceNet) {
        const { workspace } = this.state
        workspace.open(net, true);
        this.setState({ workspace })
    }

    closeNet(net: WorkspaceNet) {
        const { workspace } = this.state
        if (!net.saved) {
            if (confirm(`${net.fileName} not saved. Continue without saving?`))
                workspace.reloadNet(net)
            else return
        }
        workspace.closeNet(net);
        if (workspace.currentNet != null)
            this.setState({ workspace })
        else
            this.setState({ workspace, action: PNAction.Select })
    }

    closeAll() {
        this.state.workspace.openNets.forEach(w => this.closeNet(w))
    }

    closeOthers(net: WorkspaceNet) {
        this.state.workspace.openNets.forEach(w => {
            if (w.id === net.id) return
            this.closeNet(w)
        })
    }

    closeSaved() {
        this.state.workspace.openNets.forEach(w => {
            if (!w.saved) return
            this.closeNet(w)
        })
    }

    thereIsAtLeastOneNetOpen(): boolean {
        return this.state.workspace.currentNet != null
    }

    setCurrentNet(currentNet: WorkspaceNet) {
        const { workspace } = this.state
        workspace.updateCurrentNet(currentNet);
        this.setState({ workspace })
    }

    updateCurrentWorkspaceNet(currentNet: WorkspaceNet) {
        const { workspace } = this.state
        workspace.updateCurrentNet(currentNet);
        this.setState({ workspace })
    }

    updateCurrentGraphicNet(gpn: GraphicPetriNet) {
        const { workspace } = this.state
        workspace.currentNet.net = gpn
        workspace.currentNet.saved = false
        this.setState({ workspace })
    }

    setAction(action: PNAction) {
        const { workspace } = this.state
        if (!workspace.currentNet) this.setState({ action: PNAction.Select })
        else this.setState({ action })
    }

    deleteNet(net: WorkspaceNet) {
        const self = this
        if (!confirm(`Delete ${net.fileName}?`)) return
        $.post('./server/scripts/delete-file.php', { fileName: net.fileName }, (data) => {
            if (data === 'file not found') {
                alert(`${net.fileName} not found`)
            } else {
                const { workspace } = self.state
                workspace.removeNet(net);
                if (workspace.currentNet != null)
                    self.setState({ workspace })
                else
                    self.setState({ workspace, action: PNAction.Select })
            }
        })
    }

    deleteFile(file: WorkspaceFile) {
        const self = this
        if (!confirm(`Delete ${file.fileName}`)) return
        $.post('./server/scripts/delete-file.php', { fileName: file.fileName }, (data) => {
            if (data === 'file not found') {
                alert(`${file.fileName} not found`)
            } else {
                const { workspace } = self.state
                workspace.removeFile(file);
                self.setState({ workspace })
            }
        })
    }

    renameNet(net: WorkspaceNet) {
        const self = this

        let res = prompt(`Rename ${net.fileName}`, net.fileName)
        if (res === undefined || res === null) return
        res = res.trim()
        if (res == "") return
        res = Util.validPnmlFileName(res)
        let newname = res + '.pnml'
        if (newname === net.fileName) return
        $.post('./server/scripts/rename-file.php', { oldname: net.fileName, newname }, (data) => {
            if (data === 'file not found')
                alert('file not found')
            else if (data === 'file already exists')
                alert(`${newname} already exists`)
            else {
                const { workspace } = self.state
                workspace.renameNet(net, newname);
                self.setState({ workspace })
            }

        })
    }

    renameFile(file: WorkspaceFile) {
        let res = prompt(`Rename ${file.fileName}`, file.fileName)
        if (res === undefined || res === null) return
        res = res.trim()
        if (res == "") return
        res = Util.validXmlFileName(res)
        let newname = res + '.xml'
        if (newname === file.fileName) return
        $.post('./server/scripts/rename-file.php', { oldname: file.fileName, newname }, (data) => {
            if (data === 'file not found')
                alert('file not found')
            else if (data === 'file already exists')
                alert(`${newname} already exists`)
            else {
                const { workspace } = this.state
                workspace.renameFile(file, newname);
                this.setState({ workspace })
            }

        })
    }

    createNet() {
        let res = prompt('File Name');
        if (res === undefined || res === null) return
        res = res.trim()
        if (res == "") return
        res = Util.validPnmlFileName(res)
        res += '.pnml'

        let gpn = new GraphicPetriNet(new PetriNet())

        $.post('./server/scripts/create-new-file.php', { fileName: res, data: gpn.petriNet.xml() }, (data) => {
            if (data === 'file exists') {
                alert(`${res} already exists`)
            } else {
                const { workspace } = this.state
                let id = workspace.addNet(gpn, res)
                workspace.open(workspace.getNetById(id), true);
                this.setState({ workspace })
            }
        })
    }

    createNetFromXml(fileName: string, xml: string) {
        const gpn = new GraphicPetriNet(new PetriNet().createFromXML(fileName, xml))

        $.post('./server/scripts/create-new-file.php', { fileName, data: gpn.petriNet.xml() }, (data) => {
            if (data === 'file exists')
                alert(`${fileName} already exists`)
            else {
                const { workspace } = this.state
                const id = workspace.addNet(gpn, fileName)
                workspace.open(workspace.getNetById(id), true)
                this.setState({ workspace })
            }
        })
    }

    createFileFromContent(fileName: string, content: string) {
        $.post('./server/scripts/create-new-file.php', { fileName, data: content }, (data) => {
            if (data === 'file exists')
                alert(`${fileName} already exists`)
            else {
                const { workspace } = this.state
                const id = workspace.addFile(new FileContent().createFromText(content), fileName)
                this.setState({ workspace })
            }
        })
    }

    saveNet(net: WorkspaceNet) {
        const self = this
        const { workspace } = this.state
        if (!net) return
        $.post('./server/scripts/save-file.php', { fileName: net.fileName, data: net.net.petriNet.xml() }, data => {
            if (data === 'file not found') {
                alert(`ERROR: File not found`)
            } else {
                workspace.currentNet.saved = true
                self.setState({ workspace })
            }
        })
    }

    /**
     * Saves the current changes to an another file and 
     * reloads the old one
     */
    saveAs(net: WorkspaceNet) {
        const { workspace } = this.state

        if (!net) return
        let res = prompt(`Save ${net.fileName} as...`, net.fileName);
        if (res === undefined || res === null) return
        res = res.trim()
        if (res == "") return
        res = Util.validPnmlFileName(res)
        res += '.pnml'

        $.post('./server/scripts/create-new-file.php', { fileName: res, data: net.net.petriNet.xml() }, data => {
            if (data === 'file exists') {
                alert('file already exists')
            } else {
                let gpn = new GraphicPetriNet(
                    new PetriNet()
                        .createFromXML(res, net.net.petriNet.xml())
                )
                let id = workspace.addNet(gpn, res)
                workspace.open(workspace.getNetById(id), true);
                workspace.reloadNet(net);
                this.setState({ workspace })
            }
        })
    }

    saveAllNets() {
        const { workspace } = this.state
        const self = this
        workspace.openNets.forEach(wn => {
            $.post('./server/scripts/save-file.php', { fileName: wn.fileName, data: wn.net.petriNet.xml() }, data => {
                if (data === 'file not found') {
                    alert(`ERROR: File not found`)
                } else {
                    wn.saved = true
                    self.setState({ workspace })
                }
            })
        })
    }

    downloadNet(net: WorkspaceNet) {
        const blob = new Blob([net.net.petriNet.xml()], { type: "text/plain;charset=utf-8" })
        FileSaver.saveAs(blob, net.fileName)
    }

    downloadFile(file: WorkspaceFile) {
        const blob = new Blob([file.content], { type: "text/plain;charset=utf-8" })
        FileSaver.saveAs(blob, file.fileName)
    }

    downloadAll() {
        const { workspace } = this.state
        let zip = new JSZip()
        workspace.allNets.forEach(wn => {
            zip.file(wn.fileName, wn.net.petriNet.xml())
        })
        workspace.allFiles.forEach(wf => {
            zip.file(wf.fileName, wf.content.content)
        })

        zip.generateAsync({ type: 'blob' })
            .then(content => {
                FileSaver.saveAs(content, 'download.zip')
            })
    }

    render() {

        const { documentHeight, documentWidth, controlPanelWidth, toolBarHeight, tabBarHeight, treeViewWidth, menuBarHeight, fileTreeViewHeight, treeViewTitleHeight } = this.state

        const { workspace } = this.state
        let canvasStyle = {
            top: toolBarHeight + tabBarHeight + menuBarHeight,
            left: treeViewWidth,
            width: documentWidth - controlPanelWidth - treeViewWidth,
            height: documentHeight - toolBarHeight - tabBarHeight - menuBarHeight
        }
        return (
            <div className="page"
                onDrop={this.onDrop}
                onDragOver={(e) => e.preventDefault()}
            >
                {/* Canvas must be under all the other page components */}

                {workspace.currentNet &&
                    <Canvas
                        openNet={workspace.currentNet}
                        updateWorkspaceNet={this.updateCurrentWorkspaceNet}
                        updateGraphicNet={this.updateCurrentGraphicNet}
                        action={this.state.action}
                        setAction={this.setAction}
                        saveNet={() => this.saveNet(workspace.currentNet)}

                        style={canvasStyle}
                    />
                }
                {!workspace.currentNet && <EmptyCanvas style={canvasStyle} />}

                <TabBar
                    nets={workspace.openNets}
                    activeNet={workspace.currentNet}
                    setSelectedNet={this.setCurrentNet}
                    saveNet={this.saveNet}
                    saveAs={this.saveAs}
                    closeNet={this.closeNet}
                    closeAll={this.closeAll}
                    closeOthers={this.closeOthers}
                    thereIsAtLeastOneNetOpen={this.thereIsAtLeastOneNetOpen()}

                    style={{
                        top: toolBarHeight + menuBarHeight,
                        left: treeViewWidth,
                        width: documentWidth - controlPanelWidth - treeViewWidth,
                        height: tabBarHeight
                    }} />



                <ControlPanel
                    openNet={workspace.currentNet}
                    updateGraphicNet={this.updateCurrentGraphicNet}

                    style={{
                        top: menuBarHeight,
                        left: documentWidth - controlPanelWidth,
                        width: controlPanelWidth,
                        height: documentHeight - menuBarHeight
                    }} />

                <NetTreeView
                    allNets={workspace.allNets}
                    currentNet={workspace.currentNet}
                    setCurrentNet={this.openNet}
                    deleteNet={this.deleteNet}
                    renameNet={this.renameNet}
                    createNet={this.createNet}
                    createNetFromXml={this.createNetFromXml}
                    downloadNet={this.downloadNet}

                    titleHeight={treeViewTitleHeight}
                    style={{
                        top: menuBarHeight,
                        left: 0,
                        width: treeViewWidth,
                        height: documentHeight - menuBarHeight - fileTreeViewHeight
                    }} />

                <FileTreeView
                    allFiles={workspace.allFiles}
                    createFileFromContent={this.createFileFromContent}
                    deleteFile={this.deleteFile}
                    renameFile={this.renameFile}
                    downloadFile={this.downloadFile}

                    titleHeight={treeViewTitleHeight}
                    style={{
                        top: documentHeight - fileTreeViewHeight,
                        left: 0,
                        width: treeViewWidth,
                        height: fileTreeViewHeight
                    }} />

                <ToolBar
                    sendAction={this.setAction}
                    action={this.state.action}
                    createNet={this.createNet}
                    saveNet={() => this.saveNet(workspace.currentNet)}

                    style={{
                        top: menuBarHeight,
                        left: treeViewWidth,
                        width: documentWidth - treeViewWidth - controlPanelWidth,
                        height: toolBarHeight
                    }} />

                <MenuBar
                    createNet={this.createNet}
                    saveNet={() => this.saveNet(workspace.currentNet)}
                    saveAllNets={this.saveAllNets}
                    saveAs={() => this.saveAs(workspace.currentNet)}
                    closeCurrentNet={() => this.closeNet(this.state.workspace.currentNet)}
                    closeAllNets={this.closeAll}
                    closeOthersNets={() => this.closeOthers(this.state.workspace.currentNet)}
                    closeSaved={this.closeSaved}
                    deleteCurrentNet={() => this.deleteNet(this.state.workspace.currentNet)}
                    renameCurrentNet={() => this.renameNet(this.state.workspace.currentNet)}
                    setAction={this.setAction}
                    action={this.state.action}
                    thereIsAtLeastOneNetOpen={this.thereIsAtLeastOneNetOpen()}
                    downloadAll={this.downloadAll}

                    style={{
                        top: 0,
                        left: 0,
                        width: documentWidth,
                        height: menuBarHeight
                    }} />
            </div>
        )
    }
}