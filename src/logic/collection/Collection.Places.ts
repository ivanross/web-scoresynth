// Imports
import { Place } from '../petri'
import { GraphicPlace } from '../graphic';
import { Collection } from '../collection';
// Exports
export { PlaceCollection }


/**
 * This Class represents a *collection* of Places
 * 
 * @class PlaceCollection
 */
class PlaceCollection implements Collection<Place> {
    public elements: Place[]

    /**
     * Creates an instance of PlaceCollection.
     * @memberof PlaceCollection
     */
    constructor() {
        this.elements = []
    }

    /**
     * Returns the array of Places
     * 
     * @readonly
     * @memberof PlaceCollection
     */
    // get elements() { return this._elements }

    /**
     * Returns the number of Places in the Collection
     * 
     * @readonly
     * @memberof PlaceCollection
     */
    get length() { return this.elements.length }



    /**
     * Add *p* in the Collection
     * 
     * @param {Place} p 
     * @memberof PlaceCollection
     */
    add(p: Place): Place {
        this.elements.push(p)
        return p
    }

    /**
     * Remove the Place equals to *p* if exists.
     * 
     * @param {Place} p 
     * @returns {void} 
     * @memberof PlaceCollection
     */
    remove(p: Place): void {
        const i = this.indexOf(p)
        if (i == -1) {
            return;
        }
        this.elements.splice(i, 1)
        // p.graphicPlace = null;

    }

    /**
     * Return the index of the Place equals to *p*
     * 
     * If *p* isn't in the Collection, return *-1*.
     * 
     * @param {Place} p 
     * @returns {number} 
     * @memberof PlaceCollection
     */
    indexOf(p: Place): number {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i] == p) return i;
        }
        return -1
    }

    /**
     * Return the index of the Place with *id*.
     * 
     * If *id* isn't the id of any Place in the Collection, return *-1*
     * 
     * @param {string} id 
     * @returns 
     * @memberof PlaceCollection
     */
    indexById(id: string): number {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].id == id) return i
        }
        return -1
    }

    /**
     * Returns the `Place` with `id` (`null` otherwise)
     * 
     * @param {string} id 
     * @returns {Place} 
     * @memberof PlaceCollection
     */
    getById(id: string): Place {
        const i = this.indexById(id)
        return i < 0 ? null : this.elements[i]
    }

    /**
     * Returns an array of `Place` with the given `name`
     * 
     * @param {string} name The given name  
     * @returns {Place[]} The array of the places with `name` as name
     * @memberof PlaceCollection 
     */
    getByName(name: string): Place[] {
        const i: number[] = this.indexesByName(name)
        let p: Place[] = []
        i.forEach((index) => {
            p.push(this.elements[index])
        })
        return p
    }

    /**
     * Return the indexes of the Places with the given `name`
     * 
     * 
     * @param {string} name The given name
     * @returns {number[]} The array with the indexes
     * @memberof PlaceCollection
     */
    indexesByName(name: string): number[] {
        let indexes: number[] = []
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].name == name) indexes.push(i)
        }
        return indexes
    }
    /**
     * Creates a new Place from coords. `name` and `id` will be
     * automatcly created
     * 
     * @param {Coord} c Position of the new Place
     * @memberof PlaceCollection
     */
    create(c: Coord) {

        // Find next available id
        let newId = ""
        for (let i = 1; ; i++) {
            newId = "p" + i
            if (!this.getById(newId)) break
        }

        // Find next available name
        let newName = "P"
        for (let i = 1; ; i++) {
            newName = "P" + i
            if (this.getByName(newName).length == 0) break
        }

        let gp = new GraphicPlace(new Place(newId), c.x, c.y)
        gp.place.name = newName

        this.add(gp.place)

    }

    clone(): PlaceCollection {
        let pc = new PlaceCollection()
        this.elements.forEach(p => {
            pc.add(p.clone())
        })
        return pc
    }
}