// Exports
export { Collection }

interface Collection<G> {
    elements: G[]
    length: number

    add(g: G): G

    remove(g: G): void
    indexOf(g: G): number

    indexById(id: string): number
    getById(id: string): G

    clone(): Collection<G>
}

