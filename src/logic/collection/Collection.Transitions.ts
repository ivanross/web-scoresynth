// Imports
import { Transition } from '../petri'
import { GraphicTransition } from '../graphic';
import { Collection } from '../collection';
// Exports
export { TransitionCollection }


class TransitionCollection implements Collection<Transition> {
    public elements: Transition[]

    constructor() {
        this.elements = []
    }

    // get elements() { return this._elements }
    get length() { return this.elements.length }



    add(t: Transition): Transition {
        this.elements.push(t)
        return t;
    }

    remove(t: Transition): void {
        const i = this.indexOf(t)
        if (i == -1) {
            return
        }
        this.elements.splice(i, 1)
        // t.graphicTransition = null
    }

    indexOf(t: Transition) {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i] == t) return i;
        }
        return -1
    }

    indexById(id: string) {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].id == id) return i
        }
        return -1
    }

    getById(id: string) {
        const i = this.indexById(id)
        return i < 0 ? null : this.elements[i]
    }

    /**
     * Return the indexes of the Transitions with the given `name`
     * 
     * @param {string} name The given name
     * @returns {number[]} The array with the indexes
     * @memberof TransitionCollection
     */
    indexesByName(name: string): number[] {
        let indexes: number[] = []
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].name == name) indexes.push(i)
        }
        return indexes
    }

    getByName(name: string) {
        let t: Transition[] = []
        this.indexesByName(name).forEach((i) => {
            t.push(this.elements[i])
        })
        return t
    }

    create(c: Coord, horizontal: boolean) {
        // Find next available id
        let newId = ""
        for (let i = 1; ; i++) {
            newId = "t" + i
            if (!this.getById(newId)) break
        }

        // Find next available name
        let newName = ""
        for (let i = 1; ; i++) {
            newName = "T" + i
            if (this.getByName(newName).length == 0) break
        }

        let gt = new GraphicTransition(new Transition(newId), c.x, c.y)
        gt.transition.name = newName
        gt.isHorizontal = horizontal

        this.add(gt.transition)
    }

    clone(): TransitionCollection {
        let tc = new TransitionCollection
        this.elements.forEach(t => {
            tc.add(t.clone())
        })
        return tc
    }


}


