// Imports
import { Arc, Place, Transition, ArcType } from '../petri'
import { GraphicArc } from '../graphic';
import { Collection } from '../collection';
// Exports
export { ArcCollection }

class ArcCollection implements Collection<Arc> {

    /**
     * The elements in the collection
     */
    public elements: Arc[]

    /**
     *Creates an instance of ArcCollection.
     */
    constructor() {
        this.elements = []
    }

    /**
     * The number of the elements in the collection
     *
     * @readonly
     */
    get length() { return this.elements.length }

    /**
     * Creates a new `Arc` from `Place`, `Transition`, `ArcType` and points, then adds
     * it to the collection.
     * 
     * Returns the created `Arc`
     */
    add(x: Place, t: Transition, type: ArcType, points: [number, number][]): Arc;
    /**
     * Adds the `Arc` to the collection
     * 
     * Returns the added `Arc`
     */
    add(x: Arc): Arc;
    add(x: any, t?: any, type?: any, points?: any): Arc {
        // Arc passed
        if (x instanceof Arc) {
            this.elements.push(x)
            return x;
        }
        // Place, Transition & ArcType passed
        // Already exists
        const i = this.arcIndex(x, t, type)
        if (i >= 0) {
            this.elements[i].tokensWeight++
            return this.elements[i]
        }
        // Not Exists
        // Find next available id
        let newId = ""
        for (let i = 1; ; i++) {
            newId = "a" + i
            if (!this.getById(newId)) break
        }
        let ga = new GraphicArc(new Arc(newId, x, t, type))
        ga.pointsMiddle = points
        this.add(ga.arc)
        return ga.arc
    }

    /**
     * Remove the `Arc` from the collection if it exists
     */
    remove(a: Arc): void {
        const i = this.indexOf(a)
        if (i == -1) return
        this.elements.splice(i, 1)
    }

    /**
     * Removes an `Arc` from its index
     */
    removeAt(index: number): void {
        if (index < 0 || index >= this.elements.length) return
        this.elements.splice(index, 1)
    }

    /**
     * Returns the index of the passed `Arc`. Returns `-1` if it isn't in the collection
     */
    indexOf(a: Arc): number {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i] == a) return i;
        }
        return -1
    }

    /**
     * Returns the index of the `Arc` with the passed `id`. Returns `-1` if it isn't in the collection
     */
    indexById(id: string): number {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].id == id) return i
        }
        return -1
    }

    /**
     * Returns the `Arc` in the collection by its `id` 
     */
    getById(id: string): Arc {
        const i = this.indexById(id)
        return i < 0 ? null : this.elements[i]
    }

    /**
     * Returns the `id` of the `Arc` that has `p` as Place, `t` as Transition and 
     * `at` as ArcType
     */
    arcIndex(p: Place, t: Transition, at: ArcType): number {
        for (var i = 0; i < this.length; i++) {
            if (this.elements[i].place.id == p.id &&
                this.elements[i].transition.id == t.id &&
                this.elements[i].type == at) return i
        }
        return -1
    }

    /**
     * Returns the `Arc` that has `p` as Place, `t` as Transition and 
     * `at` as ArcType
     */
    getArc(p: Place, t: Transition, at: ArcType): Arc {
        const i = this.arcIndex(p, t, at)
        return i < 0 ? null : this.elements[i]
    }


    /**
     * Returns 
     *
     * @param {Place} p
     * @returns {Arc[]}
     * @memberof ArcCollection
     */
    getByPlace(p: Place): Arc[] {
        let arcs: Arc[] = []
        this.elements.forEach((a, i) => {
            if (a.place == p) arcs.push(a)
        })
        return arcs
    }

    removeByPlace(p: Place) {
        this.getByPlace(p).forEach((a) => {
            this.remove(a)
        })
    }

    getByTransition(t: Transition): Arc[] {
        let arcs: Arc[] = []
        this.elements.forEach((a, i) => {
            if (a.transition == t) arcs.push(a)
        })
        return arcs
    }

    removeByTransition(t: Transition): void {
        this.getByTransition(t).forEach((a) => {
            this.remove(a)
        })
    }

    clone(): ArcCollection {
        let ac = new ArcCollection()
        this.elements.forEach(a => {
            ac.add(a.clone())
        })
        return ac;
    }

}

