import { GraphicPetriNet, GraphicArc, GraphicPlace, GraphicTransition } from "../graphic";

export class WorkspaceNet {
    public net: GraphicPetriNet
    public fileName: string
    public id: string
    public isOpen: number
    public transform: TransformData
    public selectedElement: GraphicArc | GraphicPlace | GraphicTransition | null
    public saved: boolean


    constructor(id: string, net: GraphicPetriNet, fileName: string) {
        this.net = net
        this.id = id
        this.isOpen = -1
        this.transform = {
            scale: 1,
            x: 0, y: 0
        }
        this.fileName = fileName
        this.selectedElement = null
        this.saved = true
    }
}