import * as $ from 'jquery'

export class WorkspaceFile {
    constructor(public id: string, public fileName: string, public content: FileContent) { }
}

export class FileContent {

    public content: string;
    constructor() {
        this.content = ''
    }

    loadFromFile(fileName: string): this {
        $.post('./server/scripts/read-file.php', { fileName }, data => {
            const res = JSON.parse(data)
            if (res[0] == 'error') alert(res[1])
            else if (res[0] == 'content') {
                this.createFromText(res[1])
            }
        })
        return this
    }

    createFromText(content: string): this {
        this.content = content;
        return this
    }
}