import { GraphicPetriNet, GraphicArc, GraphicPlace, GraphicTransition } from "../graphic";
import { PetriNet } from "../petri";
import { WorkspaceNet, WorkspaceFile, FileContent } from "../workspace";
import * as $ from 'jquery'

export class Workspace {
    private _allNets: WorkspaceNet[]
    private _currentNet: WorkspaceNet
    private _allFiles: WorkspaceFile[]
    public baseDir: string


    constructor() {
        this._allNets = []
        this._allFiles = []
        this._currentNet = null
        this.baseDir = "./"
    }

    get allNets() { return this._allNets }
    get currentNet() { return this._currentNet }
    get allFiles() { return this._allFiles }
    get openNets() {
        return this.allNets
            .filter((wn) => { return wn.isOpen >= 0 })
            .sort((w1, w2) => { return w1.isOpen - w2.isOpen })
    }
    /**
     * Add a net to workspace
     * 
     * @returns {string} The `id` of the `WorkspaceNet`added
     */
    addNet(gpn: GraphicPetriNet, fileName: string): string {
        let id = ""
        for (let i = 1; ; i++) {
            id = "wn" + i
            if (!this.getNetById(id)) break
        }

        this._allNets.push(new WorkspaceNet(id, gpn, fileName));
        return id
    }

    addFile(content: FileContent, fileName: string): string {
        let id = ""
        for (let i = 1; ; i++) {
            id = "wf" + i
            if (!this.getFileById(id)) break
        }
        this._allFiles.push(new WorkspaceFile(id, fileName, content))
        return id
    }

    /**
     * Creates and adds `WorspaceNet` from the file name. 
     * Returns the `id` of the new net added.
     */
    addNetFromFileName(file: string): string {
        return this.addNet(
            new GraphicPetriNet(
                new PetriNet()
                    .loadFromFile(`${this.baseDir}/${file}`)
            ),
            file)
    }

    addFileFromFileName(fileName: string): string {
        return this.addFile(
            new FileContent().loadFromFile(fileName),
            fileName
        )
    }

    /**
     * Reads the net from his file and replaces the existing one,
     * without saving the changes from the las saving
     */
    reloadNet(wn: WorkspaceNet) {
        let index = this.indexNetById(wn.id)
        if (index < 0) return
        let newNet = new WorkspaceNet(
            wn.id,
            new GraphicPetriNet(new PetriNet().loadFromFile(`${this.baseDir}/${wn.fileName}`)),
            wn.fileName
        )
        this._allNets[index] = newNet
    }

    updateCurrentNet(wn: WorkspaceNet) {
        let index = this.indexNetById(wn.id);
        if (index < 0) return
        this._currentNet = wn
        this._allNets[index] = wn
    }

    open(wn: WorkspaceNet, setCurrent: boolean) {
        let index = this.indexNetById(wn.id)
        if (index < 0) return

        if (this._allNets[index].isOpen < 0)
            this._allNets[index].isOpen = this.getOpenIndex()

        if (setCurrent || !this._currentNet)
            this._currentNet = this._allNets[index]
    }

    closeNet(wn: WorkspaceNet) {
        let index = this.indexNetById(wn.id)
        if (index < 0) return

        this._allNets[index].isOpen = -1
        this._allNets[index].selectedElement = null
        this._allNets[index].transform = {
            scale: 1, x: 0, y: 0
        }
        this.reorderOpenNetIndexes()

        // wn was currentNet
        if (this.currentNet && this.currentNet.id == wn.id) {
            // check if there are open nets
            if (this.openNets.length == 0) {
                this._currentNet = null
            } else {
                this.updateCurrentNet(this.firstOpenNet)
            }
        }
    }

    removeNet(wn: WorkspaceNet) {
        let index = this.indexNetById(wn.id)
        if (index < 0) return
        this.closeNet(wn)
        this._allNets.splice(index, 1)
    }

    removeFile(wf: WorkspaceFile) {
        let index = this.indexFileById(wf.id)
        if (index < 0) return
        this._allFiles.splice(index, 1)
    }

    renameNet(wn: WorkspaceNet, newName: string) {
        let index = this.indexNetById(wn.id)
        if (index < 0) return
        this._allNets[index].fileName = newName
    }

    renameFile(wf: WorkspaceFile, newName: string) {
        let index = this.indexFileById(wf.id)
        if (index < 0) return
        this._allFiles[index].fileName = newName
    }


    get matrixData(): any {
        return this.allNets.map((n) => {
            return [n.id, n.net.petriNet.name, n.isOpen]
        }).concat(
            this.allFiles.map(f => {
                return [f.id, f.fileName]
            })
        )
    }

    get firstOpenNet(): WorkspaceNet {
        if (this.openNets.length != 0)
            return this.openNets[0]
        return null
    }

    private reorderOpenNetIndexes(): void {
        let oi = 0
        this.openNets.forEach((wn) => {
            let i = this.indexNetById(wn.id)
            if (this._allNets[i].isOpen != oi) {
                this._allNets[i].isOpen = oi
            }
            oi++
        })
    }

    private getOpenIndex(): number {
        let index = -1
        for (let wn of this.allNets) {
            if (wn.isOpen > index)
                index = wn.isOpen
        }
        return index + 1
    }

    getNetById(id: string): WorkspaceNet {
        const i = this.indexNetById(id)
        return i < 0 ? null : this._allNets[i]
    }

    getFileById(id: string): WorkspaceFile {
        const i = this.indexFileById(id)
        return i < 0 ? null : this._allFiles[i]
    }

    private indexNetById(id: string): number {
        for (let i = 0; i < this._allNets.length; i++) {
            if (this._allNets[i].id == id) return i
        }
        return -1
    }

    private indexFileById(id: string): number {
        for (let i = 0; i < this._allFiles.length; i++)
            if (this._allFiles[i].id == id) return i
        return -1
    }
}
