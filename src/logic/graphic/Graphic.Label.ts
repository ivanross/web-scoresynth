// Exports
export { Label }

const { round } = Math

class Label {
    public fontFamily: string
    private _offX: number;
    private _offY: number;
    public style: "italic" | null;
    public weight: "bold" | null;
    public decoration: "underline" | "line-through" | null

    constructor(offX: number, offY: number) {
        this.offX = offX
        this.offY = offY
        this.fontFamily = "Verdana"
        this.style = null
        this.weight = null
        this.decoration = null
        // TODO: Font Size (add parsing in PetriNet::createFromXML)
        // TODO: rotation
    }

    get offX() { return this._offX }
    set offX(ox: number) { this._offX = round(ox) }
    
    get offY() { return this._offY }
    set offY(oy: number) { this._offY = round(oy) }
}