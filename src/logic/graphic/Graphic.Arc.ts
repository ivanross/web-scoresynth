// Imports
import { Arc, ArcType, Place, Transition } from '../petri'
import { Label } from '../graphic';
const d3 = require('d3')

// Exports
export { GraphicArc, DrawingArcInfo }


interface DrawingArcInfo {
    points: [number, number][],
    type: ArcType,
    place: Place,
    transition: Transition
}

const { max, round, min, sqrt, abs } = Math

class GraphicArc {

    /**
     * Middle points of the arc. 
     * 
     * @type {[number, number][]}
     * @memberof GraphicArc
     */
    public pointsMiddle: [number, number][]
    private _strokeWidth: number
    public isInExecution: boolean
    public arcCreationInProgress: boolean

    /**
     * Color
     * 
     * @type {string}
     * @memberof GraphicArc
     */
    public stroke: string
    public curveType: number
    private _arc: Arc
    private _tension: number

    public weightLabel: Label


    static get defaults() {
        return {
            stroke: "#000000",
            strokeWidth: 2
        }
    }
    constructor(a: Arc) {
        this.arc = a
        this.tension = 1
        this.strokeWidth = GraphicArc.defaults.strokeWidth
        this.stroke = GraphicArc.defaults.stroke
        this.pointsMiddle = []
        this.isInExecution = false
        this.arcCreationInProgress = false
        this.weightLabel = new Label(0, 0)

    }

    get arc() { return this._arc }
    set arc(a: Arc) {
        this._arc = a
        if (this._arc.graphicArc != this)
            this._arc.graphicArc = this
    }
    /**
     * The **tension** of arc (for `Cardinal` curve).
     * Automatic value check:
     * > 0 <= `tension` <= 1  
     * 
     * @memberof GraphicArc
     */
    get tension() { return this._tension }
    set tension(t: number) {
        this._tension = round(max(0, min(1, t)) * 100) / 100
    }
    /**
     * The **width** of the arc. Automatic value check:
     * > `strokewidth` >= 1
     * 
     * @memberof GraphicArc
     */
    get strokeWidth() { return this._strokeWidth }
    set strokeWidth(sw: number) {
        this._strokeWidth = round(max(sw, 1))
    }


    get pointOnPlace(): [number, number] {

        // Find point right before circle
        let prevPoint: [number, number]
        if (this.pointsMiddle.length === 0)
            prevPoint = this.arc.transition.graphicTransition.point
        else if (this.arc.type === ArcType.FromPlaceToTransition)
            prevPoint = this.pointsMiddle[0]
        else
            prevPoint = this.pointsMiddle[this.pointsMiddle.length - 1]

        // https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
        let x0 = this.arc.place.graphicPlace.x,
            y0 = this.arc.place.graphicPlace.y,
            x1 = prevPoint[0],
            y1 = prevPoint[1]

        let scale = this.arc.type == ArcType.FromTransitionToPlace ? 3.18 : 0

        let r = this.arc.place.graphicPlace.radius + (this.arc.place.graphicPlace.strokeWidth) / 2 + this.strokeWidth * scale

        let d = Math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)
        let t = r / d
        if (t >= 1) return [x0, y0]
        return [
            (1 - t) * x0 + t * x1,
            (1 - t) * y0 + t * y1
        ]
    }

    get pointOnTransition(): [number, number] {
        let prevPoint: [number, number]
        if (this.pointsMiddle.length === 0)
            prevPoint = this.arc.place.graphicPlace.point
        else if (this.arc.type === ArcType.FromTransitionToPlace
        )
            prevPoint = this.pointsMiddle[0]
        else
            prevPoint = this.pointsMiddle[this.pointsMiddle.length - 1]

        // https://stackoverflow.com/a/1585620/8971140

        let Rx = this.arc.transition.graphicTransition.x
        let Ry = this.arc.transition.graphicTransition.y

        let scale = this.arc.type == ArcType.FromPlaceToTransition ? 7 : 0

        let w = this.arc.transition.graphicTransition.width + (this.arc.transition.graphicTransition.strokeWidth) / 2 + this.strokeWidth * scale
        let h = this.arc.transition.graphicTransition.height + (this.arc.transition.graphicTransition.strokeWidth) / 2 + this.strokeWidth * scale

        let Px = prevPoint[0] - Rx
        let Py = prevPoint[1] - Ry

        // Slope of line from Point P to Center
        let Pm = Py / Px

        // Slope of rect Diagonal
        let Rm = h / w

        let res: [number, number] = [0, 0]

        if (!(abs(Px) < w / 2 && abs(Py) < h / 2)) {
            // Calculate point if P has Px >= 0 && Py >= 0
            if (abs(Pm) <= Rm) {
                res[0] = w / 2
                res[1] = (w * abs(Pm)) / 2
            } else {
                res[0] = h / (abs(Pm) * 2)
                res[1] = h / 2
            }

            if (Px < 0) res[0] *= -1
            if (Py < 0) res[1] *= -1
        }

        return [res[0] + Rx, res[1] + Ry]
    }

    /**
     * The point referred by the label
     * 
     * @readonly
     * @type {[number,number]}
     * @memberof GraphicArc
     */
    get pointRefLabel(): [number, number] {
        let n = this.pointsAll.length
        if (n % 2 == 1) {
            return this.pointsAll[(n - 1) / 2]
        }
        return [
            (this.pointsAll[n / 2 - 1][0] + this.pointsAll[n / 2][0]) / 2,
            (this.pointsAll[n / 2 - 1][1] + this.pointsAll[n / 2][1]) / 2
        ]

    }

    /**
     * Return the coord of first point
     * 
     * @readonly
     * @type {[number,number]}
     * @memberof GraphicArc
     */
    get first(): [number, number] {
        if (this._arc.type == ArcType.FromPlaceToTransition) {
            return this.pointOnPlace
        }
        return this.pointOnTransition
    }

    /**
     * Return the coord of last point
     * 
     * @readonly
     * @type {[number, number]}
     * @memberof GraphicArc
     */
    get last(): [number, number] {
        if (this._arc.type == ArcType.FromTransitionToPlace) {
            return this.pointOnPlace
        }
        return this.pointOnTransition
    }

    get pointsAll() {
        return [this.first].concat(this.pointsMiddle).concat([this.last])
    }

    get svgPath(): string {
        return this.svgPathCardinal;
    }

    get svgPathLine(): string {
        return d3.line()(this.pointsAll)
    }

    get svgPathCardinal(): string {
        // https://stackoverflow.com/questions/50020079/why-marker-doesnt-orientate-as-the-path
        let points = this.pointsForCardinal().slice(0, -1)
        let path = d3.line().curve(d3.curveCardinal.tension(this.tension))(points)
        path += `L${this.pointsForCardinal()[this.pointsForCardinal().length - 1]}`

        return path
    }
 
    private pointsForCardinal(): [number, number][] {
        // Moving point
        let Ax = this.pointsAll[this.pointsAll.length - 2][0]
        let Ay = this.pointsAll[this.pointsAll.length - 2][1]

        // Last Point
        let Bx = this.pointsAll[this.pointsAll.length - 1][0]
        let By = this.pointsAll[this.pointsAll.length - 1][1]

        // Distance
        let d = 1

        // Moving Point Translated
        let Px = Ax - Bx
        let Py = Ay - By

        // Slope
        let m = Py / Px

        // Angle in first quadrant
        let ang = Math.atan(Math.abs(m))

        function sign(n: number) {
            return n > 0 ? 1 : -1
        }
        // New Point
        let res: [number, number] = [0, 0]
        res = [d * Math.cos(ang) * sign(Px), d * Math.sin(ang) * sign(Py)]
        res = [res[0] + Bx, res[1] + By]

        let cardinalPoints = this.pointsAll
        cardinalPoints.splice(-1, 0, res)
        return cardinalPoints
    }
}