// Imports
import { Place } from '../petri'
import { Label } from '../graphic'
// Exports
export { GraphicPlace }

const { round, max } = Math
class GraphicPlace {

    private _place: Place
    private _radius: number
    private _strokeWidth: number

    public stroke: string
    public fill: string
    private _x: number
    private _y: number

    public nameLabel: Label
    private nameLabelPositionModified: boolean

    public isInExecution: boolean
    public arcCreationInProgress: boolean

    static get defaults() {
        return {
            radius: 20,
            strokeWidth: 3,
            stroke: "#000000", // black
            fill: "#ffffff"    //lightcoral
        }
    }
    constructor(p: Place, posx: number = 100, posy: number = 100) {

        this.place = p

        this._radius = GraphicPlace.defaults.radius
        this._strokeWidth = GraphicPlace.defaults.strokeWidth
        this.stroke = GraphicPlace.defaults.stroke
        this.fill = GraphicPlace.defaults.fill
        this.x = posx
        this.y = posy

        this.nameLabel = new Label(0, - this._radius - 10)
        this.nameLabelPositionModified = false

        this.isInExecution = false
        this.arcCreationInProgress = false
    }

    get place() { return this._place }
    set place(p: Place) {
        this._place = p
        if (this._place.graphicPlace != this) {
            this._place.graphicPlace = this
        }
    }

    // Circle Radius
    get radius() { return this._radius }
    set radius(r: number) {
        this._radius = round(max(r, 1))
        if (!this.nameLabelPositionModified) {
            this.nameLabel.offY = - r - 10
        }
    }

    get x() { return this._x }
    set x(x: number) { this._x = round(x) }

    get y() { return this._y }
    set y(y: number) { this._y = round(y) }

    get strokeWidth() { return this._strokeWidth }
    set strokeWidth(sw: number) {
        this._strokeWidth = round(max(sw, 0))
    }

    /**
     * Return the coordinates for the center.
     * 
     * @readonly
     * @type {[number,number]}
     * @memberof GraphicPlace
     */
    get point(): [number, number] {
        return [this.x, this.y]
    }

    /**
     * Returns the **font size** of *Capacity* and 
     * *nTokens* labels inside the circle
     * 
     * @readonly
     * @type {number}
     * @memberof GraphicPlace
     */
    get fontSize(): number { return this.radius * 0.7 }
}
