// Imports
import { Transition } from '../petri'
import { Label } from '../graphic'
// Exports
export { GraphicTransition }

const { round, pow, max } = Math

class GraphicTransition {

    // public id: string
    private _transition: Transition
    public stroke: string
    private _strokeWidth: number
    public fill: string
    private _x: number
    private _y: number

    private _height: number
    private _width: number
    private _isHorizontal: boolean

    public nameLabel: Label
    private nameLabelPositionModified: boolean

    public isInExecution: boolean
    public arcCreationInProgress: boolean

    /**
     * height/width
     * 
     * @readonly
     * @private
     * @static
     * @type {number}
     * @memberof GraphicTransition
     */
    private static get ratio(): number { return 3.0 }
    static get defaults() {
        return {
            stroke: "#000000",
            fill: '#ffffff', // lightblue
            strokeWidth : 3,
            isHorizontal: false,
            height: 40,
            width: 40 / GraphicTransition.ratio
        }
    }
    constructor(t: Transition, posx: number = 100, posy: number = 100) {
        this.transition = t
        this.strokeWidth = GraphicTransition.defaults.strokeWidth

        this._isHorizontal = GraphicTransition.defaults.isHorizontal
        this.height = GraphicTransition.defaults.height

        this.stroke = GraphicTransition.defaults.stroke
        this.fill = GraphicTransition.defaults.fill
        this.x = posx
        this.y = posy

        this.nameLabel = new Label(0, - this.height/2 - 15)
        this.nameLabelPositionModified = false

        this.isInExecution = false
        this.arcCreationInProgress = false
    }

    get transition() { return this._transition }
    set transition(t: Transition) {
        this._transition = t
        if (this._transition.graphicTransition != this) {
            this._transition.graphicTransition = this
        }
    }

    get height() { return this._height }
    set height(h: number) {
        this._height = max(round(h), this.isHorizontal ? 1 : GraphicTransition.ratio)
        this._width = round(this._height * pow(GraphicTransition.ratio, this.isHorizontal ? 1 : -1))
    }

    get width() { return this._width }
    set width(w: number) {
        this._width = max(round(w), this.isHorizontal ? GraphicTransition.ratio : 1)
        this._height = round(this._width * pow(GraphicTransition.ratio, this.isHorizontal ? -1 : 1))
    }

    get isHorizontal() { return this._isHorizontal }
    set isHorizontal(isH: boolean) {
        this._isHorizontal = isH
        // Redundant Checks
        if ((isH && this.height > this.width) || (!isH && this.height < this.width)) {
            this.height = this.width
        }
    }
    get strokeWidth() { return this._strokeWidth }
    set strokeWidth(sw: number) {
        this._strokeWidth = round(max(sw, 0))
    }

    get x() { return this._x }
    set x(x: number) { this._x = round(x) }

    get y() { return this._y }
    set y(y: number) { this._y = round(y) }

    /**
     * Returns the coordinates for the center.
     * 
     * @readonly
     * @type {[number, number]}
     * @memberof GraphicTransition
     */
    get point(): [number, number] {
        return [this.x, this.y]
    }
}