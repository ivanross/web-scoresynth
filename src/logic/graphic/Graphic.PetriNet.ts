// Import
import { PetriNet, Place, Transition} from '../petri'
import { GraphicPlace, GraphicTransition} from '../graphic'
// Exports
export { PNAction, GraphicPetriNet }

enum PNAction {
    Select,
    AddPlace,
    AddVerticalTransition,
    AddHorizontalTransition,
    AddArc,
    Eraser
}

class GraphicPetriNet {

    // offset: [number,number]
    private _petriNet: PetriNet
    private _isInExecution: boolean
    private _currentAction: PNAction

    private arcPoints: [number, number][]
    private arcFrom: Place | Transition
    private arcTo: Place | Transition
    private arcCreationInProgress: boolean

    constructor(pn: PetriNet) {
        //this.offset = [0,0]

        this._isInExecution = false
        this.petriNet = pn
        this._currentAction = null

        this.arcCreationInProgress = false
        this.arcFrom = null
        this.arcTo = null
        this.arcPoints = []
    }

    get petriNet() { return this._petriNet }
    set petriNet(pn: PetriNet) {
        this._petriNet = pn
        if (this._petriNet.graphicPetriNet != this)
            this._petriNet.graphicPetriNet = this
    }

    get isInExecution() { return this._isInExecution }
    set isInExecution(b: boolean) {
        this._isInExecution = b
        for (let p of this.petriNet.places.elements) {
            p.graphicPlace.isInExecution = b
        }
        for (let t of this.petriNet.transitions.elements) {
            t.graphicTransition.isInExecution = b
        }
        for (let a of this.petriNet.arcs.elements) {
            a.graphicArc.isInExecution = b
        }
    }

    get currentAction() { return this._currentAction }
    set currentAction(act: PNAction) {
        this._currentAction = act
        if (act == PNAction.AddArc) {
            for (let p of this.petriNet.places.elements) {
                p.graphicPlace.arcCreationInProgress = true
            }
            for (let t of this.petriNet.transitions.elements) {
                t.graphicTransition.arcCreationInProgress = true
            }
            for (let a of this.petriNet.arcs.elements) {
                a.graphicArc.arcCreationInProgress = true
            }
        } else {
            for (let p of this.petriNet.places.elements) {
                p.graphicPlace.arcCreationInProgress = false
            }
            for (let t of this.petriNet.transitions.elements) {
                t.graphicTransition.arcCreationInProgress = false
            }
            for (let a of this.petriNet.arcs.elements) {
                a.graphicArc.arcCreationInProgress = false
            }
        }
    }

    removeArcFromOrTo(e: GraphicPlace | GraphicTransition) {
        for (var i = 0; i < this.petriNet.arcs.length; i++) {
            if (e instanceof GraphicPlace) {
                if (this.petriNet.arcs.elements[i].place == e.place) {


                    this.petriNet.arcs.remove(this.petriNet.arcs.elements[i]);
                    i--;
                }
            } else {
                if (this.petriNet.arcs.elements[i].transition == e.transition) {
                    this.petriNet.arcs.remove(this.petriNet.arcs.elements[i]);
                    i--
                }

            }
        }
    }

}