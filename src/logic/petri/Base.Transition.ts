// Imports
import * as $ from 'jquery'
import { GraphicTransition, } from '../graphic'
import { VtuAmountsRec } from '../petri'
import * as Util from '../../util/string-util'
// Exports
export { TransitionType, TransitionStatus, Transition }

enum TransitionType {
    Normal,
    Subnet
}

enum TransitionStatus {
    Null,
    Alternative,
    Executable,
    NotExcecutable,
    Conflict,
    AlternativeConflict
}

const { round, max } = Math

class Transition {

    public readonly id: string
    public type: TransitionType

    private _isInput: boolean
    private _isOutput: boolean
    private _file: string
    private _name: string
    private _vtuScale: number
    private _graphicTransition: GraphicTransition

    public inputObject: any;
    public inputObjectType: string;
    public status: TransitionStatus

    public fileTheme: string;
    public fileThemeOccurrence: string;
    public fileSegment: string;
    public vtuAmountsArray: VtuAmountsRec[];

    public pnmlFileName: string;

    constructor(id: string) {
        this.id = id
        this.isInput = false
        this.isOutput = false
        this.name = ""
        this.file = ""
        this.type = TransitionType.Normal
        this.vtuScale = 1

        // at start must be null
        this._graphicTransition = null
    }

    get name() { return this._name }
    set name(n: string) {
        this._name = n.trim()
    }

    get isInput() { return this._isInput }
    set isInput(is: boolean) {
        this._isInput = is
        // if (is) this._isOutput = false
    }

    get isOutput() { return this._isOutput }
    set isOutput(is: boolean) {
        this._isOutput = is
        // if (is) this._isInput = false
    }

    get file() { return this._file }
    set file(f: string) {
        this._file = f
        this.type = Util.endsWith(f, '.pnml')
            ? TransitionType.Subnet
            : TransitionType.Normal
    }

    get vtuScale() { return this._vtuScale }
    set vtuScale(value: number) { this._vtuScale = round(max(0, value)) }

    get graphicTransition() { return this._graphicTransition }
    set graphicTransition(gt: GraphicTransition) {
        this._graphicTransition = gt
        if (this._graphicTransition.transition != this) {
            this._graphicTransition.transition = this
        }
    }
    
    clone(): Transition {
        let t = new Transition(this.id);
        t.file = this.file
        t.isInput = this.isInput
        t.isOutput = this.isOutput
        t.name = this.name
        t.status = this.status
        t.type = this.type
        t.pnmlFileName = this.pnmlFileName
        t.vtuScale = this.vtuScale
        t.inputObjectType = this.inputObjectType
        t.inputObject = this.inputObject
        return t
    }


}