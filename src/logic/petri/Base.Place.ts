// Imports
import { GraphicPlace } from '../graphic'
import * as Util from '../../util/string-util'
// Exports
export { Place, PlaceType }

/* Enum PlaceType */
enum PlaceType {
    Normal,
    Subnet
}

const { round, max } = Math

/* Class Place */
class Place {

    // Attributi


    public readonly id: string
    public type: PlaceType

    private _file: string
    private _vtuScale: number
    private _graphicPlace: GraphicPlace
    private _name: string
    private _nTokens: number
    private _capacity: number
    private _isInput: boolean
    private _isOutput: boolean

    // ??
    public pnmlFileName: string

    public freeTokens: number;
    public inTokens: number;
    public outTokens: number;
    public inputObject: any;
    public inputObjectType: string;

    private _fileTheme: string
    public fileThemes: string[]
    public fileThemeOccurrence: string
    public fileThemeOccurrences: string[]
    public fileSegment: string
    public vtuAmountsArray: number[]
    public vtuAmountsStringsArray: string[]

    // Constructor
    constructor(id: string) {
        this.id = id
        this.nTokens = 0
        this.capacity = 1
        this.isInput = false
        this.isOutput = false
        this.name = ""
        this.file = ""
        this.type = PlaceType.Normal
        this.vtuScale = 1

        // at start must be null
        this._graphicPlace = null

    }


    // Methods

    get nTokens() { return this._nTokens }
    set nTokens(nt: number) {
        this._nTokens = round(max(0, nt))
        if (this._nTokens > this.capacity) this.capacity = this._nTokens
    }

    get capacity() { return this._capacity }
    set capacity(c: number) {
        this._capacity = round(max(1, c))
        if (this._capacity < this.nTokens) this.nTokens = this._capacity
    }

    get name() { return this._name }
    set name(n: string) {
        this._name = n.trim()
    }

    /**
     * Sets this place as the input node of a net. 
     * Automaticly set `isOutput` too. 
     * 
     * ***Warning***: This doesn't change anything in the net
     * 
     * 
     * @memberof Place
     */
    get isInput() { return this._isInput }
    set isInput(is: boolean) {
        this._isInput = is
        // if (is) this._isOutput = false
    }

    get isOutput() { return this._isOutput }
    set isOutput(is: boolean) {
        this._isOutput = is
        // if (is) this._isInput = false
    }


    get file() { return this._file }
    set file(value: string) {
        this._file = value
        this.type = Util.endsWith(value, '.pnml')
            ? PlaceType.Subnet
            : PlaceType.Normal
    }

    get fileTheme() { return this._file }
    set fileTheme(value: string) {
        this._fileTheme = value
        // TODO: Parse file information
    }

    get vtuScale() { return this._vtuScale }
    set vtuScale(value: number) { this._vtuScale = round(max(0, value)) }

    get graphicPlace() { return this._graphicPlace }
    set graphicPlace(gp: GraphicPlace) {
        this._graphicPlace = gp
        if (this._graphicPlace.place != this) {
            this._graphicPlace.place = this
        }
    }


    get duration(): number {
        if (this.vtuScale == 0) return 0

        // TODO: implements

        return null
    }

    clone(): Place {
        let p = new Place(this.id)
        p.capacity = this.capacity
        p.file = this.file
        p.freeTokens = this.freeTokens
        p.inTokens = this.inTokens
        p.isInput = this.isInput
        p.isOutput = this.isOutput
        p.name = this.name
        p.nTokens = this.nTokens
        p.outTokens = this.outTokens
        // TODO: track
        p.type = this.type
        p.pnmlFileName = this.pnmlFileName
        p.vtuScale = this.vtuScale
        p.inputObjectType = this.inputObjectType
        p.inputObject = this.inputObject

        return p
    }
}
