// Imports
import { Place, Transition } from '../petri';
import { GraphicArc } from '../graphic';
// Exports
export { ArcType, Arc }

enum ArcType {
    FromPlaceToTransition,
    FromTransitionToPlace
}

const { round, max } = Math

class Arc {

    public readonly id: string
    public place: Place
    public transition: Transition
    public type: ArcType
    public pnmlFileName: string
    private _tokensWeight: number
    private _probWeight: number
    private _graphicArc: GraphicArc

    constructor(id: string, p: Place, t: Transition, type: ArcType) {
        this.id = id
        this.place = p
        this.transition = t
        this.type = type

        this.tokensWeight = 1
        this.probWeight = 1

        this._graphicArc = null
    }

    get tokensWeight() { return this._tokensWeight }
    set tokensWeight(tw: number) { this._tokensWeight = round(max(1, tw)) }

    get probWeight() { return this._probWeight }
    set probWeight(pw: number) { this._probWeight = round(max(0, pw)) }

    get graphicArc() { return this._graphicArc }
    set graphicArc(ga: GraphicArc) {
        this._graphicArc = ga
        if (this._graphicArc.arc != this) {
            this._graphicArc.arc = this
        }
    }

    clone(): Arc {
        return new Arc(
            this.id,
            this.place, 
            this.transition, 
            this.type
        )

    }
}


