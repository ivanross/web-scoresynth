// Imports
import { Place, Transition, Arc, ArcType, PlaceType, TransitionType } from '../petri'
import { GraphicPetriNet, GraphicPlace, GraphicTransition, GraphicArc } from '../graphic'
import { PlaceCollection, TransitionCollection, ArcCollection } from '../collection'
import * as $ from 'jquery';
import * as convert from 'xml-js'
import * as xmlbuilder from 'xmlbuilder';
// Exports
export { VtuAmountsRec, PetriNet }

class VtuAmountsRec {
    constructor(public num: number, public den: number, public vtuAmount: number) { }

    get string() {
        return `${this.num}/${this.den}:   ${this.vtuAmount}`
    }
}

type PetriNode = Place | Transition;

class PetriNet {

    private _graphicPetriNet: GraphicPetriNet
    public places: PlaceCollection
    public transitions: TransitionCollection
    public arcs: ArcCollection

    private _inNode: PetriNode
    private _outNode: PetriNode

    public compile: boolean
    public file: string
    public name: string

    constructor() {

        this._graphicPetriNet = null
        this.places = new PlaceCollection()
        this.transitions = new TransitionCollection()
        this.arcs = new ArcCollection()

        this.inNode = null
        this.outNode = null

        this.compile = false
        this.file = ""
        this.name = ""
    }

    get graphicPetriNet() { return this._graphicPetriNet }
    set graphicPetriNet(gpn: GraphicPetriNet) {
        this._graphicPetriNet = gpn
        if (this._graphicPetriNet.petriNet != this)
            this._graphicPetriNet.petriNet = this
    }
    /**
     * The input node (in a subnet)
     * 
     * @type {PetriNode}
     * @memberof PetriNet
     */
    get inNode(): PetriNode { return this._inNode }
    set inNode(v: PetriNode) {
        let { places, transitions } = this

        // Update Places
        places.elements.map((el) => {
            el.isInput = false
            if (v && el.id == v.id) {
                el.isInput = true
            }
            if (v && v instanceof Transition && el.isOutput) {
                el.isOutput = false
                this._outNode = null
            }
            return el
        })
        this.places = places

        // Update Transitions
        transitions.elements.map((el) => {
            el.isInput = false
            if (v && el.id == v.id) {
                el.isInput = true
            }
            if (v && v instanceof Place && el.isOutput) {
                el.isOutput = false
                this._outNode = null
            }
            return el
        })
        this.transitions = transitions

        this._inNode = v
    }

    /**
     * The output node (in a subnet) 
     * 
     * @type {PetriNode}
     * @memberof PetriNet
     */
    get outNode(): PetriNode { return this._outNode }
    set outNode(v: PetriNode) {

        let { places, transitions } = this

        // Update Places
        places.elements.map((el) => {
            el.isOutput = false
            if (v && el.id == v.id) {
                el.isOutput = true
            }
            if (v && v instanceof Transition && el.isInput) {
                el.isInput = false
                this._inNode = null
            }
            return el
        })
        this.places = places

        // Update Transitions
        transitions.elements.map((el) => {
            el.isOutput = false
            if (v && el.id == v.id) {
                el.isOutput = true
            }
            if (v && v instanceof Place && el.isInput) {
                el.isInput = false
                this._inNode = null
            }
            return el
        })
        this.transitions = transitions

        this._outNode = v
    }

    updateIsInput(el: GraphicPlace | GraphicTransition, is: boolean) {
        let node: PetriNode = el instanceof GraphicPlace ? el.place : el.transition
        if (is) {
            this.inNode = node
        } else if (this.inNode == node) {
            this.inNode = null
        }
    }

    updateIsOutput(el: GraphicPlace | GraphicTransition, is: boolean) {
        let node: PetriNode = el instanceof GraphicPlace ? el.place : el.transition
        if (is) {
            this.outNode = node
        } else if (this.outNode == node) {
            this.outNode = null
        }
    }



    /**
     * Returns `id`s of all places
     * 
     * @readonly {string[]}
     * @memberof PetriNet
     */
    get placesId(): string[] {
        let id: string[] = []
        this.places.elements.forEach(p => id.push(p.id))
        return id
    }

    /**
     * Returns `id`s of all transitions
     * 
     * @readonly {string[]}
     * @memberof PetriNet
     */
    get transitionsId() {
        let id: string[] = []
        this.transitions.elements.forEach(p => id.push(p.id))
        return id
    }

    /**
     * Returns `id`s of all arcs
     * 
     * @readonly {string[]}
     * @memberof PetriNet
     */
    get arcsId() {
        let id: string[] = []
        this.arcs.elements.forEach(p => id.push(p.id))
        return id
    }


    inPlaces(t: Transition): PlaceCollection {
        let inPlaces = new PlaceCollection()
        for (let a of this.arcs.elements) {
            if (a.transition == t && a.type == ArcType.FromPlaceToTransition)
                inPlaces.add(a.place)
        }
        return inPlaces
    }

    outPlaces(t: Transition): PlaceCollection {
        let outPlaces = new PlaceCollection()
        for (let a of this.arcs.elements) {
            if (a.transition == t && a.type == ArcType.FromTransitionToPlace)
                outPlaces.add(a.place)
        }
        return outPlaces
    }

    inTransitions(p: Place): TransitionCollection {
        let inTransitions = new TransitionCollection()
        for (let a of this.arcs.elements) {
            if (a.place == p && a.type == ArcType.FromTransitionToPlace)
                inTransitions.add(a.transition)
        }
        return inTransitions
    }

    outTransitions(p: Place): TransitionCollection {
        let outTransitions = new TransitionCollection()
        for (let a of this.arcs.elements) {
            if (a.place == p && a.type == ArcType.FromPlaceToTransition)
                outTransitions.add(a.transition)
        }
        return outTransitions
    }

    /**
     * Returns the arc in the net that goes from the Place `from`
     * to the Transition `to`
     * 
     * @param {Place} from The arc beginning
     * @param {Transition} to The arc ending
     * @returns {Arc} The arc
     * @memberof PetriNet
     */
    getArc(from: Place, to: Transition): Arc;
    /**
     * Returns the arc in the net that goes from the Transition `from` 
     * to the Place `to`
     * 
     * @param {Transition} from The arc beginning
     * @param {Place} to The arc ending
     * @returns {Arc} The arc
     * @memberof PetriNet
     */
    getArc(from: Transition, to: Place): Arc;
    getArc(from: any, to: any): Arc {
        if (from instanceof Place)
            return this.arcs.getArc(from, to, ArcType.FromPlaceToTransition)
        return this.arcs.getArc(to, from, ArcType.FromTransitionToPlace)
    }

    /**
     * Remove `p` and the arcs relative to it
     * 
     * @param {Place} p The Place to be removed
     * @memberof PetriNet
     */
    removePlace(p: Place) {
        this.arcs.removeByPlace(p)
        this.places.remove(p)
        if (p.isInput)
            this._inNode = null
        if (p.isOutput)
            this._outNode = null
    }

    removeTransition(t: Transition) {
        this.arcs.removeByTransition(t)
        this.transitions.remove(t)
        if (t.isInput)
            this._inNode = null
        if (t.isOutput)
            this._outNode = null
    }

    removeArc(a: Arc) {
        this.arcs.remove(a)
    }

    loadFromFile(file: string): this {
        $.get(file, (data) => {
            // let pn = new PetriNet(file)
            this.createFromXML(file, data)
        }, 'text')
        return this

    }



    /**
     * Create the PetriNet from a `.pnml` file. 
     * 
     * @param {string} xml The text from a `.pnml` file
     * @memberof PetriNet
     */
    createFromXML(fileName: string, xml: string): this {
        // Using: xml-js v1.6.2
        let data = convert.xml2js(xml, { compact: true }).pnml.net



        ////////////
        // PLACES //
        ////////////
        if (data.place) {
            // Create always an array
            let places: any[] = []
            if (data.place.forEach) {
                places = data.place
            } else
                places = [data.place]

            places.forEach((p: any) => {
                let np = new Place(p._attributes.id)
                let ngp: GraphicPlace = new GraphicPlace(np)

                np.pnmlFileName = fileName

                ///////////
                // LOGIC //
                ///////////

                // IsInput
                if (p.isInput && p.isInput.text && p.isInput.text._text) {
                    np.isInput = p.isInput.text._text.trim() == "true"
                    if (np.isInput) this.inNode = np
                }

                // IsOutput
                if (p.isOutput && p.isOutput.text && p.isOutput.text._text) {
                    np.isOutput = p.isOutput.text._text.trim() == "true"
                    if (np.isOutput) this.outNode = np
                }

                // nTokens
                if (p.initialMarking && p.initialMarking.text && p.initialMarking.text._text)
                    np.nTokens = +p.initialMarking.text._text

                // Capacity
                if (p.capacity && p.capacity.text && p.capacity.text._text)
                    np.capacity = +p.capacity.text._text

                // Name
                if (p.name && p.name.text && p.name.text._text)
                    np.name = p.name.text._text

                // MX File
                if (p.mxFile && p.mxFile.text && p.mxFile.text._text) {
                    np.file = p.mxFile.text._text
                    let dotPos = np.file.lastIndexOf(".")
                    if (dotPos > -1 && np.file.substr(dotPos).toLowerCase() == '.pnml') {
                        np.type = PlaceType.Subnet
                    }
                }

                // TODO: TrackInfo

                // VTU Scale
                if (p.vtuScale && p.vtuScale.text && p.vtuScale.text._text)
                    np.vtuScale = p.vtuScale.text._text

                //////////////
                // GRAPHICS //
                //////////////
                if (p.graphics) {
                    if (p.graphics.position && p.graphics.position._attributes) {
                        // X
                        if (+p.graphics.position._attributes.x)
                            ngp.x = +p.graphics.position._attributes.x

                        // Y
                        if (+p.graphics.position._attributes.y)
                            ngp.y = +p.graphics.position._attributes.y
                    }
                    // Radius
                    if (p.graphics.dimension && p.graphics.dimension._attributes && p.graphics.dimension._attributes.x)
                        ngp.radius = +p.graphics.dimension._attributes.x


                    if (p.graphics.line && p.graphics.line._attributes) {
                        // Stroke Width
                        if (p.graphics.line._attributes.width)
                            ngp.strokeWidth = p.graphics.line._attributes.width

                        // Stroke
                        if (p.graphics.line._attributes.color)
                            ngp.stroke = p.graphics.line._attributes.color
                    }

                    // Fill
                    if (p.graphics.fill && p.graphics.fill._attributes && p.graphics.fill._attributes.color)
                        ngp.fill = p.graphics.fill._attributes.color
                }

                ////////////////
                // NAME LABEL //
                ////////////////
                if (p.name && p.name.graphics) {

                    if (p.name.graphics.font && p.name.graphics.font._attributes) {

                        // Font Family
                        if (p.name.graphics.font._attributes.family)
                            ngp.nameLabel.fontFamily = p.name.graphics.font._attributes.family

                        // Style
                        if (p.name.graphics.font._attributes.style && p.name.graphics.font._attributes.style.toLowerCase() == "italic")
                            ngp.nameLabel.style = 'italic'

                        // Weight
                        if (p.name.graphics.font._attributes.weight && p.name.graphics.font._attributes.weight.toLowerCase() == "bold")
                            ngp.nameLabel.weight = 'bold'

                        // Descoration
                        if (p.name.graphics.font._attributes.decoration) {
                            if (p.name.graphics.font._attributes.decoration.toLowerCase() == "underline")
                                ngp.nameLabel.decoration = 'underline'
                            else if (p.name.graphics.font._attributes.decoration.toLowerCase() == 'line-through')
                                ngp.nameLabel.decoration = 'line-through'
                        }

                    }

                    if (p.name.graphics.offset && p.name.graphics.offset._attributes) {
                        // X
                        if (p.name.graphics.offset._attributes.x)
                            ngp.nameLabel.offX = (+p.name.graphics.offset._attributes.x)

                        // Y
                        if (p.name.graphics.offset._attributes.y)
                            ngp.nameLabel.offY = (+p.name.graphics.offset._attributes.y)
                    }
                }

                this.places.add(np)
            })
        }

        /////////////////
        // TRANSITIONS //
        /////////////////
        if (data.transition) {
            // Create always an array
            let transitions: any[] = []
            if (data.transition.forEach) {
                transitions = data.transition
            } else
                transitions = [data.transition]

            transitions.forEach((t: any) => {
                let nt = new Transition(t._attributes.id)
                let ngt = new GraphicTransition(nt)

                nt.pnmlFileName = fileName

                ///////////
                // LOGIC //
                ///////////

                // IsInput
                if (t.isInput && t.isInput.text) {
                    nt.isInput = t.isInput.text._text.trim() == "true"
                    if (nt.isInput) this.inNode = nt
                }

                // IsOutput
                if (t.isOutput && t.isOutput.text) {
                    nt.isOutput = t.isOutput.text._text.trim() == "true"
                    if (nt.isOutput) this.outNode = nt
                }

                // Name
                if (t.name && t.name.text && t.name.text._text)
                    nt.name = t.name.text._text


                // MX File
                if (t.mxFile && t.mxFile.text && t.mxFile.text._text) {
                    nt.file = t.mxFile.text._text
                    let dotPos = nt.file.lastIndexOf(".")
                    if (dotPos > -1 && nt.file.substr(dotPos).toLowerCase() == '.pnml') {
                        nt.type = TransitionType.Subnet
                    }
                }

                // VTU Scale
                if (t.vtuScale && t.vtuScale.text && t.vtuScale.text._text)
                    nt.vtuScale = t.vtuScale.text._text

                //////////////
                // GRAPHICS //
                //////////////
                if (t.graphics) {
                    if (t.graphics.position && t.graphics.position._attributes) {
                        // X
                        if (+t.graphics.position._attributes.x)
                            ngt.x = +t.graphics.position._attributes.x

                        // Y
                        if (+t.graphics.position._attributes.y)
                            ngt.y = +t.graphics.position._attributes.y
                    }


                    if (t.graphics.dimension && t.graphics.dimension._attributes && t.graphics.dimension._attributes.x && t.graphics.dimension._attributes.y) {

                        let w = +t.graphics.dimension._attributes.x
                        let h = +t.graphics.dimension._attributes.y

                        // Orientation
                        ngt.isHorizontal = w > h

                        // Height (automaticly sets Width according to
                        // height and orientation)
                        ngt.height = h
                    }

                    if (t.graphics.line && t.graphics.line._attributes) {
                        // Stroke Width
                        if (t.graphics.line._attributes.width)
                            ngt.strokeWidth = t.graphics.line._attributes.width

                        // Stroke
                        if (t.graphics.line._attributes.color)
                            ngt.stroke = t.graphics.line._attributes.color
                    }

                    // Fill
                    if (t.graphics.fill && t.graphics.fill._attributes && t.graphics.fill._attributes.color)
                        ngt.fill = t.graphics.fill._attributes.color
                }

                ////////////////
                // NAME LABEL //
                ////////////////
                if (t.name && t.name.graphics) {

                    if (t.name.graphics.font && t.name.graphics.font._attributes) {

                        // Font Family
                        if (t.name.graphics.font._attributes.family)
                            ngt.nameLabel.fontFamily = t.name.graphics.font._attributes.family

                        // Style
                        if (t.name.graphics.font._attributes.style && t.name.graphics.font._attributes.style.toLowerCase() == "italic")
                            ngt.nameLabel.style = 'italic'

                        // Weight
                        if (t.name.graphics.font._attributes.weight && t.name.graphics.font._attributes.weight.toLowerCase() == "bold")
                            ngt.nameLabel.weight = 'bold'

                        // Descoration
                        if (t.name.graphics.font._attributes.decoration) {
                            if (t.name.graphics.font._attributes.decoration.toLowerCase() == "underline")
                                ngt.nameLabel.decoration = 'underline'
                            else if (t.name.graphics.font._attributes.decoration.toLowerCase() == 'line-through')
                                ngt.nameLabel.decoration = 'line-through'
                        }
                    }

                    if (t.name.graphics.offset && t.name.graphics.offset._attributes) {
                        // X
                        if (t.name.graphics.offset._attributes.x)
                            ngt.nameLabel.offX = (+t.name.graphics.offset._attributes.x)

                        // Y
                        if (t.name.graphics.offset._attributes.y)
                            ngt.nameLabel.offY = (+t.name.graphics.offset._attributes.y)
                    }
                }

                this.transitions.add(nt)

            })
        }

        //////////
        // ARCS //
        //////////
        if (data.arc) {
            // Create always an array
            let arcs: any[] = []
            if (data.arc.forEach) {
                arcs = data.arc
            } else
                arcs = [data.arc]
            arcs.forEach((a: any) => {


                ///////////
                // LOGIC //
                ///////////


                let arcId = a._attributes.id

                // Source Id
                if (!a._attributes.source) return;
                let inId = a._attributes.source

                // Target Id
                if (!a._attributes.target) return
                let outId = a._attributes.target


                // PLACE
                let arcType: ArcType = ArcType.FromPlaceToTransition
                let place: Place = this.places.getById(inId)
                if (!place) {
                    place = this.places.getById(outId)
                    arcType = ArcType.FromTransitionToPlace
                }

                // TRANSITION
                let transition: Transition = this.transitions.getById(
                    arcType == ArcType.FromPlaceToTransition ? outId : inId
                )

                // ArcType & Id
                let na = new Arc(arcId, place, transition, arcType)
                let nga = new GraphicArc(na)

                // Tokens Weight
                if (a.tokensWeight && a.tokensWeight.text && a.tokensWeight.text._text)
                    na.tokensWeight = +a.tokensWeight.text._text

                // Prob Wheight 
                if (a.probWeight && a.probWeight.text && a.probWeight.text._text)
                    na.probWeight = +a.probWeight.text._text

                na.pnmlFileName = fileName
                ///////////
                // LABEL //
                ///////////


                if (a.probWeight && a.probWeight.graphics) {
                    if (a.probWeight.graphics.offset && a.probWeight.graphics.offset._attributes) {

                        // X
                        if (a.probWeight.graphics.offset._attributes.x)
                            nga.weightLabel.offX = +a.probWeight.graphics.offset._attributes.x

                        // Y
                        if (a.probWeight.graphics.offset._attributes.y)
                            nga.weightLabel.offY = +a.probWeight.graphics.offset._attributes.y
                    }


                    if (a.probWeight.graphics.font && a.probWeight.graphics.font._attributes) {


                        // TODO: Size

                        // Font Family
                        if (a.probWeight.graphics.font._attributes.family)
                            nga.weightLabel.fontFamily = a.probWeight.graphics.font._attributes.family

                        // Style
                        if (a.probWeight.graphics.font._attributes.style && a.probWeight.graphics.font._attributes.style.toLowerCase() == "italic")
                            nga.weightLabel.style = 'italic'

                        // Weight
                        if (a.probWeight.graphics.font._attributes.weight && a.probWeight.graphics.font._attributes.weight.toLowerCase() == "bold")
                            nga.weightLabel.weight = 'bold'

                        // Descoration
                        if (a.probWeight.graphics.font._attributes.decoration) {
                            if (a.probWeight.graphics.font._attributes.decoration.toLowerCase() == "underline")
                                nga.weightLabel.decoration = 'underline'
                            else if (a.probWeight.graphics.font._attributes.decoration.toLowerCase() == 'line-through')
                                nga.weightLabel.decoration = 'line-through'
                        }


                    }
                }

                //////////////
                // GRAPHICS //
                //////////////

                // Tension 
                if (a.tension && a.tension.text && a.tension.text._text)
                    nga.tension = +a.tension.text._text



                if (a.graphics) {
                    // Points
                    if (a.graphics.position) {

                        // Create always an array
                        let points: any[] = []
                        if (a.graphics.position.forEach) {
                            points = a.graphics.position
                        } else
                            points = [a.graphics.position]

                        points.forEach((p: any) => {
                            nga.pointsMiddle.push([
                                +p._attributes.x,
                                +p._attributes.y
                            ])
                        })
                    }

                    if (a.graphics.line && a.graphics.line._attributes) {
                        // Stroke Width
                        if (a.graphics.line._attributes.width)
                            nga.strokeWidth = +a.graphics.line._attributes.width

                        // Stroke
                        if (a.graphics.line._attributes.color)
                            nga.stroke = a.graphics.line._attributes.color
                    }
                }

                this.arcs.add(na)
            })
        }
        return this
    }

    xml(pretty: boolean = true): string {
        let xml = xmlbuilder.create('pnml', { 'version': '1.0', 'encoding': 'utf-8' })
        // .att('xmlns','ciaone')
        // xml.dec('1.0', 'UTF-8', false);

        let net = xml.ele('net',
            {
                'id': 'n1',
                'type': 'http://www.maxentia.com/pnml/mxNet'
            });
        net.ele('text', this.name)
        net.up().att('xmlns', '')

        for (let e of this.places.elements) {
            let xmlEl = net.ele('place', { 'id': e.id })

            xmlEl.ele('isInput').ele('text', e.isInput ? "true" : "false")

            xmlEl.ele('isOutput').ele('text', e.isOutput ? "true" : "false")

            let name = xmlEl.ele('name')
            name.ele('text', e.name)
            let nameGraphics = name.ele('graphics')
            nameGraphics.ele('offset', {
                'x': e.graphicPlace.nameLabel.offX,
                'y': e.graphicPlace.nameLabel.offY
            })
            let font = nameGraphics.ele('font', {
                'family': e.graphicPlace.nameLabel.fontFamily
            });
            if (e.graphicPlace.nameLabel.style == "italic")
                font.att('style', 'italic')
            if (e.graphicPlace.nameLabel.weight == "bold")
                font.att('weight', 'bold')
            if (e.graphicPlace.nameLabel.decoration == "underline")
                font.att('decoration', 'underline')
            else if (e.graphicPlace.nameLabel.decoration == "line-through")
                font.att('decoration', 'line-through')

            xmlEl.ele('initialMarking').ele('text', e.nTokens)

            xmlEl.ele('capacity').ele('text', e.capacity)

            if (e.file != "")
                xmlEl.ele('mxFile').ele('text', e.file)

            // TODO: Track

            xmlEl.ele('vtuScale').ele('text', e.vtuScale)

            let graphics = xmlEl.ele('graphics')
            graphics.ele('position', {
                'x': e.graphicPlace.x,
                'y': e.graphicPlace.y
            })
            graphics.ele('dimension', {
                'x': e.graphicPlace.radius,
                'y': e.graphicPlace.radius
            })
            graphics.ele('fill', { 'color': e.graphicPlace.fill })

            graphics.ele('line', {
                'color': e.graphicPlace.stroke,
                'width': e.graphicPlace.strokeWidth
            })
        }

        for (let e of this.transitions.elements) {
            let xmlEl = net.ele('transition', { 'id': e.id })

            xmlEl.ele('isInput').ele('text', e.isInput ? "true" : "false")

            xmlEl.ele('isOutput').ele('text', e.isOutput ? "true" : "false")

            let name = xmlEl.ele('name')
            name.ele('text', e.name)
            let nameGraphics = name.ele('graphics')
            nameGraphics.ele('offset', {
                'x': e.graphicTransition.nameLabel.offX,
                'y': e.graphicTransition.nameLabel.offY
            })
            let font = nameGraphics.ele('font', {
                'family': e.graphicTransition.nameLabel.fontFamily
            });
            if (e.graphicTransition.nameLabel.style == "italic")
                font.att('style', 'italic')
            if (e.graphicTransition.nameLabel.weight == "bold")
                font.att('weight', 'bold')
            if (e.graphicTransition.nameLabel.decoration == "underline")
                font.att('decoration', 'underline')
            else if (e.graphicTransition.nameLabel.decoration == "line-through")
                font.att('decoration', 'line-through')

            if (e.file != "")
                xmlEl.ele('mxFile').ele('text', e.file)

            xmlEl.ele('vtuScale').ele('text', e.vtuScale)

            let graphics = xmlEl.ele('graphics')
            graphics.ele('position', {
                'x': e.graphicTransition.x,
                'y': e.graphicTransition.y
            })
            graphics.ele('dimension', {
                'x': e.graphicTransition.width,
                'y': e.graphicTransition.height
            })

            graphics.ele('fill', { 'color': e.graphicTransition.fill })

            graphics.ele('line', {
                'color': e.graphicTransition.stroke,
                'width': e.graphicTransition.strokeWidth
            })
        }

        for (let e of this.arcs.elements) {
            let xmlEl = net.ele('arc', { 'id': e.id })

            if (e.type == ArcType.FromPlaceToTransition) {
                xmlEl.att('source', e.place.id)
                xmlEl.att('target', e.transition.id)
            } else {
                xmlEl.att('source', e.transition.id)
                xmlEl.att('target', e.place.id)
            }

            xmlEl.ele('tokensWeight').ele('text', e.tokensWeight)

            let xmlProbWeight = xmlEl.ele('probWeight')
            xmlProbWeight.ele('text', e.probWeight)
            let pwGraphics = xmlProbWeight.ele('graphics')
            pwGraphics.ele('offset', {
                'x': e.graphicArc.weightLabel.offX,
                'y': e.graphicArc.weightLabel.offY
            })

            let font = pwGraphics.ele('font', {
                'family': e.graphicArc.weightLabel.fontFamily
            });
            // TODO: size
            if (e.graphicArc.weightLabel.style == "italic")
                font.att('style', 'italic')
            if (e.graphicArc.weightLabel.weight == "bold")
                font.att('weight', 'bold')
            if (e.graphicArc.weightLabel.decoration == "underline")
                font.att('decoration', 'underline')
            else if (e.graphicArc.weightLabel.decoration == "line-through")
                font.att('decoration', 'line-through')

            xmlEl.ele('tension').ele('text', e.graphicArc.tension)

            let graphics = xmlEl.ele('graphics')

            graphics.ele('line', {
                'color': e.graphicArc.stroke,
                'width': e.graphicArc.strokeWidth
            })

            e.graphicArc.pointsMiddle.forEach((p) => {
                graphics.ele('position', {
                    'x': p[0],
                    'y': p[1]
                })
            })
        }
        if (pretty)
            return xml.end({ pretty })
        else
            return xml.toString()
    }

    clone(): PetriNet {
        let pn = new PetriNet()
        pn.name = this.name
        pn.arcs = this.arcs.clone()
        pn.places = this.places.clone()
        pn.transitions = this.transitions.clone()

        for (let a of pn.arcs.elements) {
            a.place = pn.places.getById(a.place.id)
            a.transition = pn.transitions.getById(a.transition.id)
        }
        pn.inNode = this.inNode
        pn.outNode = this.outNode
        pn.file = this.file

        return pn
    }

    // TODO: add Timeline

    // TODO: implement ExecuteStep

    // TODO: implement setStep


}
