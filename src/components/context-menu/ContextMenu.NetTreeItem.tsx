import * as React from 'react'
import * as Menu from "../menu";

interface Props extends Menu.ContextMenuProps {
    renameNet: () => void,
    deleteNet: () => void,
    downloadNet: () => void,
    closeContextMenu: () => void
}

export class NetTreeItemContextMenu extends React.Component<Props> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >
                <Menu.MenuItem name='Rename' onClick={() => {
                    this.props.renameNet();
                    this.props.closeContextMenu()
                }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Delete' onClick={() => {
                    this.props.deleteNet();
                    this.props.closeContextMenu()
                }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Download' onClick={() => {
                    this.props.downloadNet();
                    this.props.closeContextMenu()
                }} />
            </Menu.ContextMenu>

        )
    }
}