import * as React from 'react'
import * as Menu from '../menu'

interface Props extends Menu.ContextMenuProps {
    closeAll: () => void,
    thereIsAtLeastOneNetOpen: boolean,
    closeContextMenu: () => void
}

export class TabBarContextMenu extends React.Component<Props> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut}>
                <Menu.MenuItem name='Close all'
                    onClick={() => {
                        this.props.closeAll();
                        this.props.closeContextMenu();
                    }}
                    disabled={!this.props.thereIsAtLeastOneNetOpen} />
            </Menu.ContextMenu>
        )
    }
}