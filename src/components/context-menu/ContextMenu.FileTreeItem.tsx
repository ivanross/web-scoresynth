import * as React from 'react'
import * as Menu from "../menu";

interface Props extends Menu.ContextMenuProps {
    renameFile: () => void,
    deleteFile: () => void,
    closeContextMenu: () => void,
    downloadFile: () => void
}

export class FileTreeItemContextMenu extends React.Component<Props> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >
                <Menu.MenuItem name='Rename' onClick={() => {
                    this.props.renameFile();
                    this.props.closeContextMenu()
                }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Delete' onClick={() => {
                    this.props.deleteFile();
                    this.props.closeContextMenu()
                }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Download' onClick={() => {
                    this.props.downloadFile();
                    this.props.closeContextMenu()
                }} />
            </Menu.ContextMenu>

        )
    }
}