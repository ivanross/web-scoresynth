import * as React from 'react'
import * as Menu from '../menu'

interface Props extends Menu.ContextMenuProps {
    saveNet: () => void
    saveAs: () => void,
    closeNet: () => void,
    closeAll: () => void,
    closeOthers: () => void
    closeContextMenu: () => void
}

export class TabContextMenu extends React.Component<Props, {}> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >
                <Menu.MenuItem name='Save' onClick={() => { this.props.saveNet(); this.props.closeContextMenu() }} />
                <Menu.MenuItem name='Save as' onClick={() => { this.props.saveAs(); this.props.closeContextMenu() }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Close' onClick={() => { this.props.closeNet(); this.props.closeContextMenu() }} />
                <Menu.MenuItem name='Close all' onClick={() => { this.props.closeAll(); this.props.closeContextMenu() }} />
                <Menu.MenuItem name='Close others' onClick={() => { this.props.closeOthers(); this.props.closeContextMenu() }} />
            </Menu.ContextMenu>
        )
    }

}