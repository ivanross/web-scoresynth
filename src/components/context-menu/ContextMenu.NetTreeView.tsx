import * as React from 'react'
import * as Menu from "../menu";
import * as $ from 'jquery'

interface Props extends Menu.ContextMenuProps {
    createNet: () => void,
    closeContextMenu: () => void
}

export class NetTreeViewContextMenu extends React.Component<Props, {}> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut}>
                <Menu.MenuItem name='New' onClick={() => {
                    this.props.closeContextMenu()
                    this.props.createNet();
                }} />
                <Menu.MenuItem name='Upload' onClick={() => {
                    $('#pnml-loader-input').click();
                    this.props.closeContextMenu();
                }} />
            </Menu.ContextMenu>
        )
    }
}