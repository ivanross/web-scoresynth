import * as React from 'react'
import * as Menu from '../menu'
import { PNAction } from '../../logic/graphic';

interface Props extends Menu.ContextMenuProps {
    action: PNAction,
    setAction: (action: PNAction) => void,
    saveNet: () => void,
    closeContextMenu: () => void
}

export class CanvasContextMenu extends React.Component<Props, {}> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >

                <Menu.MenuItem name="Select"
                    selected={this.props.action == PNAction.Select}
                    onClick={() => {
                        this.props.setAction(PNAction.Select);
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuItem name="Erase"
                    selected={this.props.action == PNAction.Eraser}
                    onClick={() => {
                        this.props.setAction(PNAction.Eraser)
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name="Place"
                    selected={this.props.action == PNAction.AddPlace}
                    onClick={() => {
                        this.props.setAction(PNAction.AddPlace);
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuItem name="Transition V"
                    selected={this.props.action == PNAction.AddVerticalTransition}
                    onClick={() => {
                        this.props.setAction(PNAction.AddVerticalTransition);
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuItem name="Transition H"
                    selected={this.props.action == PNAction.AddHorizontalTransition}
                    onClick={() => {
                        this.props.setAction(PNAction.AddHorizontalTransition);
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuItem name="Arc"
                    selected={this.props.action == PNAction.AddArc}
                    onClick={() => {
                        this.props.setAction(PNAction.AddArc);
                        this.props.closeContextMenu()
                    }} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Save' onClick={() => {
                    this.props.saveNet();
                    this.props.closeContextMenu()
                }} />
            </Menu.ContextMenu>
        )
    }

}