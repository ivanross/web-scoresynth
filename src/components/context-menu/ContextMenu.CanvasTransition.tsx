import * as React from 'react'
import * as Menu from '../menu'

interface Props extends Menu.ContextMenuProps {
    toggleInputNode: () => void,
    toggleOutputNode: () => void,
    isInput: boolean,
    isOutput: boolean,
    delete: () => void,
    closeContextMenu: () => void
}

export class TransitionContextMenu extends React.Component<Props, {}> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >
                <Menu.MenuItem name='Input'
                    onClick={this.props.toggleInputNode}
                    selected={this.props.isInput} />
                <Menu.MenuItem name='Output'
                    onClick={this.props.toggleOutputNode}
                    selected={this.props.isOutput} />
                <Menu.MenuSeparator />
                <Menu.MenuItem name='Delete' onClick={() => {
                    this.props.delete();
                    this.props.closeContextMenu()
                }} />
            </Menu.ContextMenu>
        )
    }
}