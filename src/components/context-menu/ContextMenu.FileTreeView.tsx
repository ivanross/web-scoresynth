import * as React from 'react'
import * as $ from 'jquery'
import * as Menu from '../menu'

interface Props extends Menu.ContextMenuProps {
    closeContextMenu: () => void
}

export class FileTreeViewContextMenu extends React.Component<Props, {}> {
    render() {
        return (
            <Menu.ContextMenu
                position={this.props.position}
                onClickOut={this.props.onClickOut} >

                <Menu.MenuItem name='Upload' onClick={() => {
                    $('#file-loader-input').click();
                    this.props.closeContextMenu()
                }} />
            </Menu.ContextMenu>
        )
    }
}