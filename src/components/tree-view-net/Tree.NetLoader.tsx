import * as React from 'react'
import * as $ from 'jquery'

interface Props {
    createNet: (fileName: string, xml: string) => void
}
export class NetLoader extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props)
        this.onFileSelected = this.onFileSelected.bind(this)
        this.onButtonClick = this.onButtonClick.bind(this)
    }

    onFileSelected(files: FileList) {
        if (files.length === 0) return
        for (let i = 0; i < files.length; i++) {
            let reader = new FileReader()
            reader.onload = (e: FileReaderProgressEvent) => {
                this.props.createNet(files[i].name, e.target.result)
            }
            reader.readAsText(files[i])
        }
    }

    onButtonClick() {
        $('#pnml-loader-input').click();
    }

    render() {
        return (
            <div className='tree-view-button'>
                <input id='pnml-loader-input'
                    type="file"
                    accept=".pnml"
                    onChange={(e) => this.onFileSelected(e.target.files)}
                    multiple
                    hidden />
                <button onClick={this.onButtonClick}>Upload</button>
            </div>
        )
    }
}