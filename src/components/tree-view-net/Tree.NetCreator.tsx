import * as React from 'react'
import * as $ from 'jquery'

interface Props {
    createNet: () => void
}
export class NetCreator extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props)
    }
    
    render() {
        return (
            <div className='tree-view-button'>
                <button onClick={this.props.createNet}>Create</button>
            </div>
        )
    }
}