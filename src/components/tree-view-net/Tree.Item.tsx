import * as React from 'react'
import { WorkspaceNet } from "../../logic/workspace";
import { MenuItem, MenuSeparator, ContextMenuPosition, ContextMenu } from '../menu';
import { NetTreeItemContextMenu } from '../context-menu';

interface Props {
    // Added to make the pure component render on rename
    fileName: string,
    net: WorkspaceNet,
    active: boolean,
    setCurrentNet: () => void,
    deleteNet: () => void,
    renameNet: () => void,
    downloadNet: () => void
}
interface State {
    contextMenuPosition: ContextMenuPosition
}

export class NetTreeItem extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = { contextMenuPosition: null }
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }

    onContextMenu(e: any) {
        e.preventDefault();
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    render() {
        const { active, net } = this.props
        const { contextMenuPosition } = this.state

        return (
            <div
                className={`tree-item ${active ? "tree-item-active" : ""}`}
                onClick={this.props.setCurrentNet}
                onContextMenu={this.onContextMenu} >

                {net.fileName}

                {contextMenuPosition != null &&
                    <NetTreeItemContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        renameNet={this.props.renameNet}
                        deleteNet={this.props.deleteNet}
                        downloadNet= {this.props.downloadNet}
                        closeContextMenu={this.closeContextMenu} />
                }
            </div>
        )
    }
}