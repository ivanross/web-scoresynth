import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as $ from 'jquery'
import { QuickPortal } from '../../util/react-util';

export interface ContextMenuPosition {
    top: number,
    left: number,
}

export interface ContextMenuProps {
    position: ContextMenuPosition,
    onClickOut: () => void
}

/**
 * **HOW TO USE**
 * 1. A custom ContextMenu must have:
 *  1. `Props` that `extends contextMenuProps` 
 *  1. All the actions in the `Props`
 * 1. The Component that uses a ContextMenu must have:
 *  1. Its `State` with `ContextMenuPosition`
 *  1. A `onContextMenu` function handler on the root to open the context menu
 * (NB: the function should have `event.preventDefault()` and `event.stopPropagation()`). This function has to set the position of the context menu
 *  1. A function to close the context menu. This function has to be assigned to `onClickOut` prop of the custom context menu
 */
export class ContextMenu extends React.PureComponent<ContextMenuProps, {}> {
    constructor(props: ContextMenuProps) {
        super(props);
    }

    // Handle open and close context menu. the event must be listened by 
    // #app to make a context menu disappear on right click 
    componentDidMount() {
        $('#app .page').bind('click', this.props.onClickOut)
        $('#app .page').bind('contextmenu', this.props.onClickOut)
    }
    componentWillUnmount() {
        $('#app .page').unbind('click', this.props.onClickOut)
        $('#app .page').unbind('contextmenu', this.props.onClickOut)
    }

    render() {
        const { position } = this.props
        return (
            <QuickPortal query={'#app'}>
                <div className='context-menu' style={position}>
                    <table>
                        <tbody>
                            {this.props.children}
                        </tbody>
                    </table>
                </div>
            </QuickPortal>
        )
    }
}


