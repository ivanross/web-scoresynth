import * as React from 'react';

export function MenuSeparator() {
    return (
        <React.Fragment>
            <tr className='menu-sep-top'>
                <td colSpan={3} />
            </tr>
            <tr className='menu-sep-bottom'>
                <td colSpan={3} />
            </tr>
        </React.Fragment>
    )
}