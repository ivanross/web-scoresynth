import * as React from 'react'

interface Props {
    name: string
}

export class MenuBarItem extends React.PureComponent<Props, {}> {
    render() {
        return (
            <div className='menu'>
                <div className='menu-title'>
                    {this.props.name}
                </div>
                <div className='menu-content'>
                    <table>
                        <tbody>
                            {this.props.children}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}