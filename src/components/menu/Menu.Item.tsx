import * as React from 'react'

interface Props {
    name: string
    shortcut?: string,
    selected?: boolean,
    disabled?: boolean,
    onClick?: () => void
}

export class MenuItem extends React.PureComponent<Props, {}> {
    static defaultProps: Partial<Props> = {
        selected: false,
        disabled: false,
        shortcut: '',
        onClick: () => { }
    }

    constructor(props: Props) {
        super(props);
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(e: any) {
        e.preventDefault()
        e.stopPropagation()
        this.props.onClick()
    }

    render() {
        let handleClick = !this.props.disabled ? this.handleClick : () => { }
        return (
            <tr className={`menu-item ${this.props.disabled ? 'menu-item-disabled' : ''}`}
                onClick={handleClick}>
                <td className='menu-item-selected'>
                    {this.props.selected && <i className="fas fa-circle fa-xs icon-mod" />}
                </td>
                <td className='menu-item-name'>
                    {this.props.name}
                </td>
                <td className='menu-item-shortcut'>
                    {this.props.shortcut}
                </td>
            </tr>
        )
    }
}