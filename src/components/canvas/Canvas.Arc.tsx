import * as React from 'react';
import * as $ from 'jquery'
import { GraphicArc, Label } from '../../logic/graphic';
import { CanvArcPoint, CanvLabel } from '../canvas';
// const d3 = require('d3')

export { CanvArc }


interface Props {
    ga: GraphicArc,
    transform: TransformData,
    updateData: (ga: GraphicArc) => void,
    sendSelected: (ga: GraphicArc) => void,
    isSelected: boolean,
    selectable: boolean
}
interface State {
    dragging: boolean,
}



class CanvArc extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)

        this.onDoubleClick = this.onDoubleClick.bind(this)
        this.onMouseDown = this.onMouseDown.bind(this)

        this.updatePointPosition = this.updatePointPosition.bind(this)
        this.updateLabelPosition = this.updateLabelPosition.bind(this)

        this.sendSelected = this.sendSelected.bind(this)

        this.deletePoint = this.deletePoint.bind(this)

        this.stringToBePrinted = this.stringToBePrinted.bind(this)

    }
    componentDidMount() {
        $(`#${this.props.ga.arc.id}`).mousedown(this.onMouseDown)
        $(`#${this.props.ga.arc.id}`).dblclick(this.onDoubleClick)
    }

    onMouseDown(e: any) {
        if (!this.props.selectable) return;
        this.props.sendSelected(this.props.ga)
    }

    onDoubleClick(e: any) {
        if (!this.props.selectable) return;

        let { ga, transform } = this.props
        let { scale, x, y } = transform

        // Canvas offset
        let co = $("#canvas").offset()

        let P: [number, number] = [
            (e.pageX - co.left - x) / scale,
            (e.pageY - co.top - y) / scale
        ]
        let points = ga.pointsAll

        function dist(A: [number, number], B: [number, number]) {
            return Math.sqrt((A[0] - B[0]) ** 2 + (A[1] - B[1]) ** 2)
        }
        function intercept(P: [number, number], slope: number) {
            return P[1] - slope * P[0]
        }

        function isBetween(x: number, a: number, b: number) {
            if (a < b) return a < x && x < b
            return b < x && x < a
        }

        let distances: number[] = []
        for (let i = 0; i < points.length - 1; i++) {
            let V = points[i]
            let W = points[i + 1]

            let VW = dist(V, W)
            let VP = dist(V, P)
            let WP = dist(W, P)
            // ERONE
            let A = (Math.sqrt((VW + VP + WP) * (- VW + VP + WP) * (VW - VP + WP) * (VW + VP - WP))) / 4

            // check if P projection is on VW
            let VWm = (W[1] - V[1]) / (W[0] - V[0])
            let m = -1 / VWm
            let Vq = intercept(V, m)
            let Wq = intercept(W, m)
            let Pq = intercept(P, m)

            if (isBetween(Pq, Wq, Vq))
                distances.push(VW === 0 ? VP : 2 * A / VW)
            else distances.push(VP > WP ? WP : VP)
        }

        // Find minor distance
        let j = 0
        distances.forEach((d, i, a) => {
            if (d < a[j]) j = i
        })

        P = [Math.round(P[0]), Math.round(P[1])]
        // Add the point
        ga.pointsMiddle.splice(j, 0, P)

        this.props.updateData(ga);
    }


    updatePointPosition(i: number): (point: [number, number]) => void {
        return (point: [number, number]) => {
            let { ga } = this.props
            ga.pointsMiddle[i] = point
            this.props.updateData(ga);
        }

    }

    updateLabelPosition(lb: Label) {
        let { ga } = this.props
        ga.weightLabel = lb
        this.props.updateData(ga)
    }

    deletePoint(i: number): () => void {
        return () => {
            let { ga } = this.props
            ga.pointsMiddle.splice(i, 1)
            this.props.updateData(ga);
        }
    }

    sendSelected() {
        this.props.sendSelected(this.props.ga)
    }

    stringToBePrinted(): string {
        const { ga } = this.props
        let print = ""
        if (ga.arc.tokensWeight != 1) print += ga.arc.tokensWeight
        print += " "
        if (ga.arc.probWeight != 1) print += "[" + ga.arc.probWeight + "]"

        return print.trim()
    }

    render() {
        const { ga, transform, isSelected } = this.props
        const labelText = this.stringToBePrinted()
        let points = ga.pointsMiddle.map((p, i) => {
            return (
                <CanvArcPoint
                    key={`ap-${i}`}
                    id={`ap-${ga.arc.id}-${i}`}
                    point={p}
                    scale={transform.scale}
                    updatePosition={this.updatePointPosition(i)}
                    delete={this.deletePoint(i)}
                    sendSelected={this.sendSelected} />
            )

        })

        let label = <CanvLabel
            parentId={ga.arc.id}
            className='label arc-label'
            value={labelText}
            label={ga.weightLabel}
            refPos={ga.pointRefLabel}
            updatePosition={this.updateLabelPosition}
            transform={transform}
            sendSelected={this.sendSelected}
            isSelected={isSelected}
            selectable={this.props.selectable}
            editable={false}
            updateValue={null} />

        let pointsLine = <path
            d={ga.svgPathLine}
            strokeWidth={1}
            stroke={'lightgrey'}
            fill="none" />


        return (
            <g id={ga.arc.id} className="pn-arc">
                <defs>
                    <marker id={`arrhead-${ga.arc.id}`} viewBox="0 0 20 20" refX="1" refY="10" orient="auto">
                        <path d="M 0 0 L 20 10 L 0 20 z" style={{ fill: ga.stroke, stroke: 'none' }} />
                    </marker>
                </defs>
                <path
                    d={ga.svgPath}
                    strokeWidth={ga.strokeWidth + 8}
                    stroke={'white'}
                    fill="none" />

                {isSelected && pointsLine}

                <path
                    d={ga.svgPath}
                    strokeWidth={ga.strokeWidth}
                    stroke={ga.stroke}
                    fill="none"
                    strokeDasharray={this.props.isSelected ? `${ga.strokeWidth * 2},${ga.strokeWidth}` : ""}
                    style={{ markerEnd: `url(#arrhead-${ga.arc.id})` }}
                />
                {isSelected && points}
                {labelText && label}
                {/* Debug Code */}
                {/* {ga.curveType == CurveType.Cardinal && ga.pointsForCardinal.map((p, i, a) => {
                    if (i < a.length - 2) return null
                    return <circle key={i} cx={p[0]} cy={p[1]} r={2} fill={i == a.length - 1 ? 'red' : 'blue'} />
                })} */}
            </g>
        )
    }
}
