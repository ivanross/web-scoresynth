import * as React from 'react';
import * as $ from 'jquery'
export { CanvArcPoint }

interface Props {
    point: [number, number],
    id: string,
    scale: number,
    updatePosition: (point: [number, number]) => void,
    delete: () => void,
    sendSelected: any
}

interface State {
    dragging: boolean,
    rel: Coord
}

class CanvArcPoint extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            dragging: false,
            rel: null
        }

        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
        this.onDoubleClick = this.onDoubleClick.bind(this)
    }

    componentDidMount() {
        $(`#${this.props.id}`).mousedown(this.onMouseDown)
        $(`#${this.props.id}`).dblclick(this.onDoubleClick)
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.dragging && !prevState.dragging) {
            $(document)
                .mousemove(this.onMouseMove)
                .mouseup(this.onMouseUp)
        } else if (!this.state.dragging && prevState.dragging) {
            $(document).unbind('mousemove').unbind('mouseup')
        }
    }

    onMouseDown(e: any) {
        this.props.sendSelected();
        // only left mouse button
        if (e.button !== 0) return;

        const { point, scale } = this.props
        this.setState({
            dragging: true,
            rel: {
                x: Math.round(e.pageX / scale - point[0]),
                y: Math.round(e.pageY / scale - point[1])
            }
        })
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseMove(e: any) {
        if (!this.state.dragging) return
        let { point, scale } = this.props
        let { rel } = this.state

        // New var needed because point in props must 
        // not change to ensure weight label position updating
        let p: [number,number] = [0,0]
        p[0] = Math.round(e.pageX / scale - rel.x)
        p[1] = Math.round(e.pageY / scale - rel.y)

        // Lift up GraphicPlace info
        this.props.updatePosition(p)

        e.stopPropagation()
        e.preventDefault()
    }

    onMouseUp(e: any) {
        this.setState({ dragging: false })
        e.stopPropagation()
        e.preventDefault()
    }

    onDoubleClick(e: any) {
        this.props.delete();
        e.preventDefault()
        e.stopPropagation()
    }

    render() {
        const { point, id } = this.props
        return (
            <circle
                id={id}
                className="arc-point"
                cx={point[0]}
                cy={point[1]}
                r={5}
            />
        )
    }
}