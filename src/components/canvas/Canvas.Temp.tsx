import * as React from 'react'
import { GraphicPlace, GraphicTransition } from '../../logic/graphic';

interface Props {
    mouse: Coord
}

export function Circle(props: Props) {
    return <circle
        fill={GraphicPlace.defaults.fill}
        r={GraphicPlace.defaults.radius}
        strokeWidth={GraphicPlace.defaults.strokeWidth}
        style={{ stroke: GraphicPlace.defaults.stroke }}
        cx={props.mouse.x}
        cy={props.mouse.y}
        opacity={0.25}
    />
}

export function RectVertical(props: Props) {
    return <rect
        x={props.mouse.x - GraphicTransition.defaults.width / 2}
        y={props.mouse.y - GraphicTransition.defaults.height / 2}
        strokeWidth={GraphicTransition.defaults.strokeWidth}
        fill={GraphicTransition.defaults.fill}
        stroke={GraphicTransition.defaults.stroke}
        height={GraphicTransition.defaults.height}
        width={GraphicTransition.defaults.width}
        opacity={.25}

    />
}

export function RectHorizontal(props: Props) {
    return <rect
        x={props.mouse.x - GraphicTransition.defaults.height / 2}
        y={props.mouse.y - GraphicTransition.defaults.width / 2}
        strokeWidth={GraphicTransition.defaults.strokeWidth}
        fill={GraphicTransition.defaults.fill}
        stroke={GraphicTransition.defaults.stroke}
        height={GraphicTransition.defaults.width}
        width={GraphicTransition.defaults.height}
        opacity={.25}
    />
}