import * as React from "react";
import { GraphicTransition, Label } from '../../logic/graphic'
import { TransitionType } from "../../logic/petri";
import * as $ from 'jquery'
import { CanvLabel } from "../canvas";
import { ContextMenuPosition } from "../menu";
import { TransitionContextMenu } from "../context-menu";
export { CanvTransition }

type Coord = { x: number, y: number }

interface Props {
    gt: GraphicTransition,
    updateData: (gt: GraphicTransition) => void
    transform: TransformData,
    sendSelected: (gt: GraphicTransition) => void,
    isSelected: boolean,
    selectable: boolean,
    updateIsInput: (is: boolean) => void,
    updateIsOutput: (is: boolean) => void,
    delete: () => void
}
interface State {
    dragging: boolean,
    rel: Coord,
    labelEditing: boolean,
    contextMenuPosition: ContextMenuPosition
}

class CanvTransition extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            dragging: false,
            rel: null,
            labelEditing: false,
            contextMenuPosition: null
        }
        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
        this.onLabelDoubleClick = this.onLabelDoubleClick.bind(this)

        this.updateLabelPosition = this.updateLabelPosition.bind(this)
        this.updateName = this.updateName.bind(this)
        this.sendSelectedFromLabel = this.sendSelectedFromLabel.bind(this)

        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
        this.toggleIsInput = this.toggleIsInput.bind(this)
        this.toggleIsOutput = this.toggleIsOutput.bind(this)
    }

    componentDidMount() {
        // Event Listeners
        $(`#${this.props.gt.transition.id}`).mousedown(this.onMouseDown)
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.dragging && !prevState.dragging) {
            $(document)
                .mousemove(this.onMouseMove)
                .mouseup(this.onMouseUp)
        } else if (!this.state.dragging && prevState.dragging) {
            $(document).unbind('mousemove').unbind('mouseup')
        }
    }
    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        if (!nextProps.isSelected) prevState.labelEditing = false
        return prevState
    }

    onMouseDown(e: any) {
        // Element Selected
        if (!this.props.selectable) return;
        this.props.sendSelected(this.props.gt)

        // only left mouse button
        if (e.button !== 0) return;

        const { gt, transform } = this.props
        const { scale } = transform
        this.setState({
            dragging: true,
            rel: {
                x: (e.pageX / scale - gt.x),
                y: (e.pageY / scale - gt.y)
            }
        })
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseMove(e: any) {
        if (!this.state.dragging) return
        let { gt, transform } = this.props
        const { scale } = transform
        let { rel } = this.state
        let d = gt

        const prevX = d.x
        const prevY = d.y

        d.x = e.pageX / scale - rel.x
        d.y = e.pageY / scale - rel.y

        // Lift up GraphicPlace info
        this.props.updateData(d)

        e.stopPropagation()
        e.preventDefault()
    }

    onMouseUp(e: any) {
        this.setState({ dragging: false })
        e.stopPropagation()
        e.preventDefault()
    }

    onLabelDoubleClick() {
        this.setState({ labelEditing: true })
    }

    updateLabelPosition(lb: Label) {
        let { gt } = this.props
        gt.nameLabel = lb
        this.props.updateData(gt)
    }

    updateName(name: string) {
        let { gt } = this.props
        gt.transition.name = name
        this.props.updateData(gt)
    }

    sendSelectedFromLabel() {
        this.props.sendSelected(this.props.gt)
    }

    onContextMenu(e: any) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    toggleIsInput() {
        const { gt } = this.props
        this.props.updateIsInput(!gt.transition.isInput)
        this.props.updateData(gt)
    }

    toggleIsOutput() {
        const { gt } = this.props
        this.props.updateIsOutput(!gt.transition.isOutput)
        this.props.updateData(gt)
    }



    render() {
        const { gt } = this.props
        const { labelEditing, contextMenuPosition } = this.state


        let longestSide = gt.height > gt.width ? gt.height / 2 : gt.width / 2;

        return (
            <g id={gt.transition.id} className="pn-transition"
                onContextMenu={this.onContextMenu}>

                {/* Display Rect */}
                <rect
                    x={gt.x - gt.width / 2}
                    y={gt.y - gt.height / 2}
                    strokeWidth={gt.strokeWidth}
                    strokeDasharray={this.props.isSelected ? `${gt.strokeWidth * 2},${gt.strokeWidth}` : ""}
                    stroke={gt.stroke}
                    width={gt.width}
                    height={gt.height}
                    fill={gt.fill} />

                {gt.transition.type === TransitionType.Subnet &&
                    <React.Fragment>
                        <defs>
                            <mask id={`mask-${gt.transition.id}`}>
                                <rect x={gt.x - gt.width / 2} y={gt.y - gt.height / 2} width={gt.width} height={gt.height} style={{ stroke: 'none', fill: '#ffffff' }} />
                            </mask>
                        </defs>
                        <g style={{ mask: `url(#mask-${gt.transition.id})` }}>
                            <line x1={gt.x - longestSide} y1={gt.y + longestSide} x2={gt.x + longestSide} y2={gt.y - longestSide} stroke={gt.stroke} strokeWidth={gt.strokeWidth} />
                            <line x1={gt.x - longestSide - longestSide / 2} y1={gt.y + longestSide} x2={gt.x + longestSide - longestSide / 2} y2={gt.y - longestSide} stroke={gt.stroke} strokeWidth={gt.strokeWidth} />
                            <line x1={gt.x - longestSide - longestSide} y1={gt.y + longestSide} x2={gt.x + longestSide - longestSide} y2={gt.y - longestSide} stroke={gt.stroke} strokeWidth={gt.strokeWidth} />
                            <line x1={gt.x - longestSide + longestSide / 2} y1={gt.y + longestSide} x2={gt.x + longestSide + longestSide / 2} y2={gt.y - longestSide} stroke={gt.stroke} strokeWidth={gt.strokeWidth} />
                            <line x1={gt.x - longestSide + longestSide} y1={gt.y + longestSide} x2={gt.x + longestSide + longestSide} y2={gt.y - longestSide} stroke={gt.stroke} strokeWidth={gt.strokeWidth} />
                        </g>
                    </React.Fragment>
                }

                {/* Display Name Label */}
                <CanvLabel parentId={gt.transition.id} className="transition-label label"
                    value={gt.transition.name}
                    label={gt.nameLabel}
                    refPos={[gt.x, gt.y]}
                    updatePosition={this.updateLabelPosition}
                    transform={this.props.transform}
                    sendSelected={this.sendSelectedFromLabel}
                    isSelected={this.props.isSelected}
                    selectable={this.props.selectable}
                    editable={true}
                    updateValue={this.updateName}
                    onDoubleClick={this.onLabelDoubleClick}
                    editing={labelEditing} />

                {contextMenuPosition != null &&
                    <TransitionContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        toggleInputNode={this.toggleIsInput}
                        toggleOutputNode={this.toggleIsOutput}
                        isInput={gt.transition.isInput}
                        isOutput={gt.transition.isOutput}
                        delete={this.props.delete}
                        closeContextMenu={this.closeContextMenu} />
                }
            </g>

        )
    }
}
