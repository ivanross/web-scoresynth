import * as React from "react";
import { Label, GraphicPlace, GraphicTransition } from '../../logic/graphic'
import * as $ from 'jquery'
import { CanvForm } from "../canvas";
export { CanvLabel }

interface Props {
    parentId: string
    className: string,
    value: string,
    label: Label,
    transform: TransformData,
    refPos: [number, number],
    updatePosition: (lb: Label) => void,
    sendSelected: () => void,
    updateValue: (value: string) => void,
    onDoubleClick?: () => void,
    isSelected: boolean,
    selectable: boolean,
    editable: boolean,
    editing?: boolean
}
interface State {
    dragging: boolean,
    rel: Coord
}

class CanvLabel extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            dragging: false,
            rel: null
        }

        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
    }

    // static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
    //     if (!nextProps.isSelected) prevState.editing = false
    //     return prevState
    // }

    componentDidMount() {
        // Label listen for MouseDown
        $(`#lb-${this.props.parentId}`).mousedown(this.onMouseDown)


        // Label listen for DoubleClick if is editable
        if (this.props.editable)
            $(`#lb-${this.props.parentId}`).dblclick(this.props.onDoubleClick)

    }
    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.dragging && !prevState.dragging) {
            $(document)
                .bind('mousemove', this.onMouseMove)
                .mouseup(this.onMouseUp)
        } else if (!this.state.dragging && prevState.dragging) {
            $(document).unbind('mousemove').unbind('mouseup')
        }

    }

    onMouseDown(e: any) {
        if (!this.props.selectable) return;
        this.props.sendSelected()

        // only left mouse button
        if (e.button !== 0) return;

        const { label, transform } = this.props
        this.setState({
            dragging: true,
            rel: {
                x: e.pageX / transform.scale - label.offX,
                y: e.pageY / transform.scale - label.offY
            }
        })
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseMove(e: any) {
        if (!this.state.dragging) return
        let { label, transform } = this.props
        let d = label

        d.offX = e.pageX / transform.scale - this.state.rel.x
        d.offY = e.pageY / transform.scale - this.state.rel.y

        this.props.updatePosition(d)

        e.stopPropagation()
        e.preventDefault()
    }

    onMouseUp(e: any) {
        this.setState({ dragging: false })
        e.stopPropagation()
        e.preventDefault()
    }

    render() {
        const { parentId, className, label, value, refPos, transform } = this.props
        let text = <text
            x={label.offX + refPos[0]}
            y={label.offY + refPos[1]}

            style={{
                fontFamily: label.fontFamily,
                fontStyle: label.style,
                fontWeight: label.weight,
                textDecoration: label.decoration
            }}>
            {value}
        </text>


        let input = <CanvForm
            x={(label.offX + refPos[0])}
            y={(label.offY + refPos[1])}
            type={'text'}
            value={value}
            updateValue={this.props.updateValue}
            transform={this.props.transform} />

        return (
            <g id={`lb-${parentId}`} className={className}>
                {text}
                {this.props.editing && input}
            </g>
        )
    }
}

