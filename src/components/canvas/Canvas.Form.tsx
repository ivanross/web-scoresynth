import * as React from 'react'
import { QuickPortal } from '../../util/react-util';
export { CanvForm }

interface Props {
    /**
     * Absolute `x` position in `div#canvas`. It's
     * automatically transformed by `transform` info
     * 
     * @type {number}
     * @memberof Props
     */
    x: number,
    /**
     * Absolute `y` position in `div#canvas`. It's
     * automatically transformed by `transform` info
     * 
     * @type {number}
     * @memberof Props
     */
    y: number,
    type: 'text' | 'number'
    value: string,
    updateValue: (value: string) => void,
    transform: TransformData,
}

interface State {
    value: string
}

class CanvForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { value: props.value }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        return { value: nextProps.value }
    }

    onChange(e: React.FormEvent<HTMLInputElement>) {
        if (this.props.type == 'number') {
            let disp = e.currentTarget.value.trim()
            if (disp.length == 0) disp = "0"
            while (disp.length > 1 && disp.charAt(0) == "0") {
                disp = disp.substr(1)
            }
            this.setState({ value: disp })
        } else
            this.setState({ value: e.currentTarget.value })
    }

    onSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        this.props.updateValue(this.state.value)
    }

    render() {
        const { x, y, transform, type } = this.props
        const { value } = this.state

        let w = 70
        return (
            <QuickPortal query="#canvas" >
                <div
                    style={{
                        position: 'absolute',
                        width: w,
                        height: 50,
                        left: (x * transform.scale + transform.x) - w / 2,
                        top: y * transform.scale + transform.y
                    }} >
                    <form onSubmit={this.onSubmit}>
                        <input type={type}
                            value={value}
                            style={{ width: w }}
                            onChange={this.onChange} />
                        <input type="submit" hidden />
                    </form>
                </div>
            </QuickPortal>
        )
    }
}