import * as React from "react";
import { GraphicPlace, Label } from '../../logic/graphic'
import * as $ from 'jquery'
import { CanvForm, CanvLabel } from "../canvas";
import { ContextMenuPosition } from "../menu";
import { PlaceContextMenu } from "../context-menu";
import { PlaceType } from "../../logic/petri";
export { CanvPlace }



interface Props {
    gp: GraphicPlace,
    updateData: (gp: GraphicPlace) => void,
    transform: TransformData,
    sendSelected: (gp: GraphicPlace) => void,
    isSelected: boolean,
    selectable: boolean,
    updateIsInput: (is: boolean) => void,
    updateIsOutput: (is: boolean) => void,
    delete: () => void
}
interface State {
    dragging: boolean,
    rel: Coord,
    editing: 'tokens' | 'capacity' | "label" | null,
    contextMenuPosition: ContextMenuPosition
}

class CanvPlace extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            dragging: false,
            rel: null,
            editing: null,
            contextMenuPosition: null
        }
        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
        this.onTokensDoubleClick = this.onTokensDoubleClick.bind(this)
        this.onCapacityDoubleClick = this.onCapacityDoubleClick.bind(this)
        this.onLabelDoubleClick = this.onLabelDoubleClick.bind(this)


        this.updateLabelPosition = this.updateLabelPosition.bind(this)
        this.updateName = this.updateName.bind(this)
        this.updateTokens = this.updateTokens.bind(this)
        this.updateCapacity = this.updateCapacity.bind(this)
        this.sendSelectedFromLabel = this.sendSelectedFromLabel.bind(this)

        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)

        this.toggleIsInput = this.toggleIsInput.bind(this)
        this.toggleIsOutput = this.toggleIsOutput.bind(this)
    }

    componentDidMount() {
        const { gp } = this.props
        // Circle listen for MouseDown
        $(`#${gp.place.id}`).mousedown(this.onMouseDown)
        $(`#ntokens-${gp.place.id}`).dblclick(this.onTokensDoubleClick)
        $(`#capacity-${gp.place.id}`).dblclick(this.onCapacityDoubleClick)
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.dragging && !prevState.dragging) {
            $(document)
                .mousemove(this.onMouseMove)
                .mouseup(this.onMouseUp)
        } else if (!this.state.dragging && prevState.dragging) {
            $(document).unbind('mousemove').unbind('mouseup')
        }
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        if (!nextProps.isSelected) prevState.editing = null
        return prevState
    }

    onMouseDown(e: any) {
        // Element Selected
        if (!this.props.selectable) return;
        this.props.sendSelected(this.props.gp)

        // only left mouse button
        if (e.button !== 0) return;
        const { gp, transform } = this.props
        const { scale } = transform

        this.setState({
            dragging: true,
            rel: {
                x: e.pageX / scale - gp.x,
                y: e.pageY / scale - gp.y
            }
        })
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseMove(e: any) {
        if (!this.state.dragging) return
        let { gp, transform } = this.props
        const { scale } = transform

        let { rel } = this.state
        let d = gp

        const prevX = d.x
        const prevY = d.y

        d.x = e.pageX / scale - rel.x
        d.y = e.pageY / scale - rel.y

        // Lift up GraphicPlace info
        this.props.updateData(d)

        e.stopPropagation()
        e.preventDefault()
    }

    onMouseUp(e: any) {
        this.setState({ dragging: false })
        e.stopPropagation()
        e.preventDefault()
    }

    onTokensDoubleClick() { this.setState({ editing: 'tokens' }) }
    onCapacityDoubleClick() { this.setState({ editing: 'capacity' }) }
    onLabelDoubleClick() { this.setState({ editing: 'label' }) }

    updateLabelPosition(lb: Label) {
        let { gp } = this.props
        gp.nameLabel = lb
        this.props.updateData(gp)
    }

    updateName(name: string) {
        let { gp } = this.props
        gp.place.name = name
        this.props.updateData(gp)
    }

    updateTokens(t: string) {
        let { gp } = this.props
        gp.place.nTokens = +t
        this.props.updateData(gp)
    }

    updateCapacity(c: string) {
        let { gp } = this.props
        gp.place.capacity = +c
        this.props.updateData(gp)
    }

    toggleIsInput() {
        const { gp } = this.props
        this.props.updateIsInput(!gp.place.isInput)
        this.props.updateData(gp)
    }

    toggleIsOutput() {
        const { gp } = this.props
        this.props.updateIsOutput(!gp.place.isOutput)
        this.props.updateData(gp)
    }

    sendSelectedFromLabel() {
        this.props.sendSelected(this.props.gp)
    }

    onContextMenu(e: any) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    render() {
        const { gp } = this.props
        const { editing, contextMenuPosition } = this.state
        const yTokens = gp.y - gp.radius * .4
        const yCapacity = gp.y + gp.radius * .4

        let formTokens = <CanvForm
            value={gp.place.nTokens + ""}
            type='number'
            x={gp.x}
            y={yTokens + 5}
            transform={this.props.transform}
            updateValue={this.updateTokens} />


        let formCapacity = <CanvForm
            value={gp.place.capacity + ""}
            type='number'
            x={gp.x}
            y={yCapacity + 5}
            transform={this.props.transform}
            updateValue={this.updateCapacity} />

        return (
            <g id={gp.place.id} className="pn-place"
                onContextMenu={this.onContextMenu}>

                {/* Display Circle */}
                <circle
                    cx={gp.x}
                    cy={gp.y}
                    r={gp.radius}
                    strokeWidth={gp.strokeWidth}
                    strokeDasharray={this.props.isSelected ? `${gp.strokeWidth * 2},${gp.strokeWidth}` : ""}
                    style={{ stroke: gp.stroke }}
                    fill={gp.fill} />

                {gp.place.type === PlaceType.Subnet &&
                    <React.Fragment>
                        <defs>
                            <mask id={`mask-${gp.place.id}`}>
                                <circle cx={gp.x} cy={gp.y} r={gp.radius} style={{ stroke: 'none', fill: '#ffffff' }} />
                            </mask>
                        </defs>

                        <g style={{ mask: `url(#mask-${gp.place.id})` }}>
                            <line x1={gp.x - gp.radius} y1={gp.y + gp.radius} x2={gp.x + gp.radius} y2={gp.y - gp.radius} stroke={gp.stroke} strokeWidth={gp.strokeWidth} />
                            <line x1={gp.x - gp.radius - gp.radius / 2} y1={gp.y + gp.radius} x2={gp.x + gp.radius - gp.radius / 2} y2={gp.y - gp.radius} stroke={gp.stroke} strokeWidth={gp.strokeWidth} />
                            <line x1={gp.x - gp.radius - gp.radius} y1={gp.y + gp.radius} x2={gp.x + gp.radius - gp.radius} y2={gp.y - gp.radius} stroke={gp.stroke} strokeWidth={gp.strokeWidth} />
                            <line x1={gp.x - gp.radius + gp.radius / 2} y1={gp.y + gp.radius} x2={gp.x + gp.radius + gp.radius / 2} y2={gp.y - gp.radius} stroke={gp.stroke} strokeWidth={gp.strokeWidth} />
                            <line x1={gp.x - gp.radius + gp.radius} y1={gp.y + gp.radius} x2={gp.x + gp.radius + gp.radius} y2={gp.y - gp.radius} stroke={gp.stroke} strokeWidth={gp.strokeWidth} />
                        </g>
                    </React.Fragment>
                }

                {/* Display nTokens Text */}
                {gp.place.type !== PlaceType.Subnet &&
                    <text id={`ntokens-${gp.place.id}`} className="ntokens-filed inner-place-field"
                        x={gp.x}
                        y={yTokens}
                        style={{ fontSize: gp.fontSize }} >
                        {gp.place.nTokens}
                    </text>
                }

                {/* Display Capacity Text */}
                {gp.place.type !== PlaceType.Subnet &&
                    <text id={`capacity-${gp.place.id}`} className="capacity-filed inner-place-field"
                        x={gp.x}
                        y={yCapacity}
                        style={{ fontSize: gp.fontSize }} >
                        {gp.place.capacity}
                    </text>
                }

                {/* Display Name Label */}
                <CanvLabel parentId={gp.place.id} className="place-label label"
                    value={gp.place.name}
                    label={gp.nameLabel}
                    refPos={[gp.x, gp.y]}
                    updatePosition={this.updateLabelPosition}
                    transform={this.props.transform}
                    sendSelected={this.sendSelectedFromLabel}
                    isSelected={this.props.isSelected}
                    selectable={this.props.selectable}
                    editable={true}
                    updateValue={this.updateName}
                    onDoubleClick={this.onLabelDoubleClick}
                    editing={editing == 'label'} />

                {editing == 'tokens' && formTokens}
                {editing == 'capacity' && formCapacity}

                {contextMenuPosition != null &&
                    <PlaceContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        toggleInputNode={this.toggleIsInput}
                        toggleOutputNode={this.toggleIsOutput}
                        isInput={gp.place.isInput}
                        isOutput={gp.place.isOutput}
                        delete={this.props.delete}
                        closeContextMenu={this.closeContextMenu} />

                }
            </g>
        )
    }
}
