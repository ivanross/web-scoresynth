import * as React from 'react'
import * as $ from 'jquery'

interface Props {
    createFile: (fileName: string, text: string) => void
}
export class FileLoader extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props)
        this.onFileSelected = this.onFileSelected.bind(this)
        this.onButtonClick = this.onButtonClick.bind(this)
    }

    onFileSelected(files: FileList) {
        if (files.length === 0) return
        for (let i = 0; i < files.length; i++) {
            let reader = new FileReader()
            reader.onload = (e: FileReaderProgressEvent) => {
                this.props.createFile(files[i].name, e.target.result)
            }
            reader.readAsText(files[i])
        }
    }

    onButtonClick() {
        $('#file-loader-input').click();
    }

    render() {
        return (
            <div className='tree-view-button'>
                <input id='file-loader-input'
                    type="file"
                    accept=".xml"
                    onChange={(e) => this.onFileSelected(e.target.files)}
                    multiple
                    hidden />
                <button onClick={this.onButtonClick}>Upload</button>
            </div>
        )
    }
}