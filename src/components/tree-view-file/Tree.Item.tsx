import * as React from 'react'
import { WorkspaceFile } from "../../logic/workspace";
import { ContextMenuPosition } from '../menu';
import { FileTreeItemContextMenu } from '../context-menu';

interface Props {
    // Added to make the pure component render on rename
    fileName: string,
    file: WorkspaceFile,
    deleteFile: () => void,
    renameFile: () => void,
    downloadFile: () => void
}
interface State {
    contextMenuPosition: ContextMenuPosition
}

export class FileTreeItem extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = { contextMenuPosition: null }
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }

    onContextMenu(e: any) {
        e.preventDefault();
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }


    render() {
        const { file } = this.props
        const { contextMenuPosition } = this.state

        return (
            <div
                className={`tree-item`}
                onContextMenu={this.onContextMenu} >

                {file.fileName}

                {contextMenuPosition &&
                    <FileTreeItemContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        closeContextMenu={this.closeContextMenu}
                        deleteFile={this.props.deleteFile}
                        renameFile={this.props.renameFile}
                        downloadFile={this.props.downloadFile} />
                }
            </div>
        )
    }
}