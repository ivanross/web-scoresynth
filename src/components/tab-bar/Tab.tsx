import * as React from 'react'
import { WorkspaceNet } from "../../logic/workspace";
import { ContextMenuPosition } from '../menu/Menu.Context';
import { TabContextMenu } from '../context-menu/ContextMenu.Tab';

interface Props {
    name: string,
    active: boolean,
    net: WorkspaceNet,
    // this makes the PureComponent render when net.currentNet.saved changes 
    saved: boolean,
    setSelectedNet: (wn: WorkspaceNet) => void,
    saveNet: () => void,
    saveAs: () => void,
    closeNet: () => void,
    closeAll: () => void,
    closeOthers: () => void
}

interface State {
    contextMenuPosition: ContextMenuPosition
}

export class Tab extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { contextMenuPosition: null }
        this.onTabClick = this.onTabClick.bind(this)
        this.onCloseButtonClick = this.onCloseButtonClick.bind(this)
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }

    onTabClick(e: any) {
        this.props.setSelectedNet(this.props.net)
        e.preventDefault()
        e.stopPropagation()
    }

    onCloseButtonClick(e: any) {
        this.props.closeNet();
        e.preventDefault()
        e.stopPropagation()
    }

    onContextMenu(e: any) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }
    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    render() {
        const { name, active, net, saved } = this.props
        const { contextMenuPosition } = this.state
        return (

            <div className={`tab ${active ? 'tab-active' : ''}`}
                onClick={this.onTabClick}
                onContextMenu={this.onContextMenu}>
                <div className="tab-side" >
                    {!saved && <i className="fas fa-circle fa-xs icon-mod"></i>}
                </div>
                <div className={`tab-name ${saved ? "" : "tab-mod"}`} >
                    {name}
                </div>
                <div className="tab-side" onClick={this.onCloseButtonClick}>
                    <i className="fas fa-times close-button" />
                </div>

                {contextMenuPosition != null &&
                    <TabContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        saveNet={this.props.saveNet}
                        saveAs={this.props.saveAs}
                        closeNet={this.props.closeNet}
                        closeAll={this.props.closeAll}
                        closeOthers={this.props.closeOthers}
                        closeContextMenu={this.closeContextMenu} />
                }
            </div>

        )
    }
}