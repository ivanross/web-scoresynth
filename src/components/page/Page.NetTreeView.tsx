import * as React from 'react'
// import { ContextMenuTrigger, ContextMenu, MenuItem } from 'react-contextmenu'
import { WorkspaceNet } from '../../logic/workspace';
import { NetTreeItem, NetLoader, NetCreator } from '../tree-view-net';
import { ContextMenuPosition } from '../menu';
import { NetTreeViewContextMenu } from '../context-menu';
export { NetTreeView }

interface Props {
    style: FixedDivStyle,
    allNets: WorkspaceNet[],
    currentNet: WorkspaceNet,
    setCurrentNet: (gpn: WorkspaceNet) => void,
    deleteNet: (net: WorkspaceNet) => void,
    renameNet: (net: WorkspaceNet) => void,
    createNet: () => void,
    createNetFromXml: (fileName: string, xml: string) => void,
    downloadNet: (net: WorkspaceNet) => void,
    titleHeight: number,
    // height: number
}


interface State {
    contextMenuPosition: ContextMenuPosition
}

class NetTreeView extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { contextMenuPosition: null }
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }
    onContextMenu(e: any) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }
    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    render() {
        const { style, allNets, currentNet, titleHeight } = this.props
        const { contextMenuPosition } = this.state
        return (
            <div className='tree-view' style={style}
                onContextMenu={this.onContextMenu} >
                <div className='tree-view-header' style={{ height: titleHeight }}>
                    NETS
                </div>

                <div className='tree-view-content' style={{ height: style.height - titleHeight }}>
                    {allNets.sort((a, b) => a.fileName.localeCompare(b.fileName)).map((n, i) => {
                        return <NetTreeItem
                            key={i}
                            net={n}
                            fileName={n.fileName}
                            active={currentNet ? n.id == currentNet.id : false}
                            setCurrentNet={() => this.props.setCurrentNet(n)}
                            deleteNet={() => this.props.deleteNet(n)}
                            renameNet={() => this.props.renameNet(n)}
                            downloadNet={() => this.props.downloadNet(n)} />
                    })}

                    {contextMenuPosition != null &&
                        <NetTreeViewContextMenu
                            position={contextMenuPosition}
                            onClickOut={this.closeContextMenu}
                            createNet={this.props.createNet}
                            closeContextMenu={this.closeContextMenu}
                            
                        />
                    }

                    <NetCreator
                        createNet={this.props.createNet} />
                        
                    <NetLoader
                        createNet={this.props.createNetFromXml} />

                </div>
            </div >
        )
    }
}
