import * as React from 'react'
import { PNAction } from '../../logic/graphic';
export { ToolBar }

interface Props {
    style: FixedDivStyle,
    action: PNAction
    sendAction: (action: PNAction) => void,
    createNet: () => void
    saveNet: () => void
}

interface State {
    currentDrawingAction: PNAction.AddPlace | PNAction.AddArc | PNAction.AddHorizontalTransition | PNAction.AddVerticalTransition
}

class ToolBar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)

        this.state = { currentDrawingAction: PNAction.AddPlace }
    }
    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        if (nextProps.action == PNAction.AddPlace ||
            nextProps.action == PNAction.AddArc ||
            nextProps.action == PNAction.AddHorizontalTransition ||
            nextProps.action == PNAction.AddVerticalTransition)
            prevState.currentDrawingAction = nextProps.action
        return prevState
    }

    shouldComponentUpdate(nextProps: Props, nextState: State) {
        if (nextProps.action !== this.props.action) return true
        if (nextState.currentDrawingAction !== this.state.currentDrawingAction) return true
        if (nextProps.style.height !== this.props.style.height) return true
        if (nextProps.style.width !== this.props.style.width) return true
        if (nextProps.style.top !== this.props.style.top) return true
        if (nextProps.style.left !== this.props.style.left) return true
        return false
    }

    render() {
        const { style, action } = this.props
        const { currentDrawingAction } = this.state
        const drawing = isDrawingAction(action)

        let currentDrawingLabel = 'Place'
        if (currentDrawingAction == PNAction.AddArc) currentDrawingLabel = 'Arc'
        if (currentDrawingAction == PNAction.AddHorizontalTransition) currentDrawingLabel = 'Transition H'
        if (currentDrawingAction == PNAction.AddVerticalTransition) currentDrawingLabel = 'Transition V'

        return (
            <div id="toolbar" style={style}>

                <button className={`tool`}
                    onClick={this.props.createNet}>
                    <i className="far fa-file tool-icon"></i>
                    New
                </button>

                <button className={`tool`}
                    onClick={this.props.saveNet}>
                    <i className="far fa-save tool-icon"></i>
                    Save
                </button>


                {/* <div className="tool-sep" /> */}

                <button className={`tool ${action == PNAction.Select ? "tool-active" : ""}`}
                    onClick={() => this.props.sendAction(PNAction.Select)}>
                    <i className="fas fa-mouse-pointer tool-icon"></i>
                    Select
                </button>

                <div className="dropdown">
                    <button className={`tool tool-group ${drawing ? "tool-active" : ""}`}
                        onClick={() => this.props.sendAction(currentDrawingAction)}>
                        <i className="fas fa-paint-brush tool-icon"></i>
                        {currentDrawingLabel}
                        <i className="fas fa-angle-down dropdown-icon"></i>
                    </button>
                    <div className="dropdown-content">
                        <div className="dropdown-item"
                            onClick={() => this.props.sendAction(PNAction.AddPlace)}>
                            Place
                            </div>
                        <div className="dropdown-item"
                            onClick={() => this.props.sendAction(PNAction.AddVerticalTransition)}>
                            Transition V
                        </div>
                        <div className="dropdown-item"
                            onClick={() => this.props.sendAction(PNAction.AddHorizontalTransition)}>
                            Transition H
                        </div>
                        <div className="dropdown-item"
                            onClick={() => this.props.sendAction(PNAction.AddArc)}>
                            Arc
                        </div>
                    </div>
                </div>

                <button className={`tool ${action == PNAction.Eraser ? "tool-active" : ""}`}
                    onClick={() => this.props.sendAction(PNAction.Eraser)}>
                    <i className="fas fa-eraser tool-icon"></i>
                    Erase
                </button>

            </div>
        )
    }
}

function isDrawingAction(action: PNAction): boolean {
    return action == PNAction.AddPlace ||
        action == PNAction.AddArc ||
        action == PNAction.AddHorizontalTransition ||
        action == PNAction.AddVerticalTransition
}