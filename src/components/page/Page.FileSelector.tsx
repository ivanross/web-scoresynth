import * as React from 'react'
import { QuickPortal } from '../../util/react-util'
import * as $ from 'jquery'

interface Props {
    closeSelector: () => void,
    sendFileName: (fileName: string) => void
}
interface State {
    files: {
        pnml: string[],
        xml: string[]
    },
    selectedFile: string
}

export class FileSelector extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = { files: null, selectedFile: '' }
        this.handleRowClick = this.handleRowClick.bind(this)
    }

    componentDidMount() {
        $.post('./server/scripts/read-file-of-users.php', data => {
            const files = JSON.parse(data)
            this.setState({ files })
        })
    }

    handleRowClick(selectedFile: string) {
        return () => {
            this.setState({ selectedFile })
        }
    }

    render() {
        const { files, selectedFile } = this.state
        return (
            <QuickPortal query='#app'>
                <div id='file-selector'>
                    <div className='file-selector-content'>
                        <FileSelectorHeader
                            close={this.props.closeSelector} />
                        <FileSelectorSection title='Nets'>
                            {files && files.pnml
                                .sort((a, b) => a.localeCompare(b))
                                .map(f =>
                                    <FileSelectorRow key={f}
                                        selected={f == selectedFile}
                                        fileName={f}
                                        onClick={this.handleRowClick(f)}
                                    />
                                )}
                        </FileSelectorSection>
                        <FileSelectorSection title='Files'>
                            {files && files.xml
                                .sort((a, b) => a.localeCompare(b))
                                .map(f =>
                                    <FileSelectorRow key={f}
                                        fileName={f}
                                        selected={f == selectedFile}
                                        onClick={this.handleRowClick(f)}
                                    />
                                )}
                        </FileSelectorSection>
                        <FileSelectorButton
                            onClick={() => this.props.sendFileName(selectedFile)}
                            disabled={selectedFile == ''} />
                    </div>
                </div>
            </QuickPortal>
        )
    }
}

function FileSelectorHeader({ close }: { close: () => void }) {
    return (
        <div className="file-selector-header">
            Select a file
            <div className="file-selector-close-button">
                <i className="fas fa-times" onClick={close} />
            </div>
        </div>
    )
}

function FileSelectorSection({ title, children }: { title: string, children: React.ReactNode }) {
    return (
        <div className="file-selector-section">
            <div className="file-selector-section-title">{title}</div>
            <div className="file-selector-section-content">{children}</div>
        </div>
    )
}

class FileSelectorRow extends React.PureComponent<{
    fileName: string,
    selected: boolean,
    onClick: () => void
}, {}> {
    render() {
        return (
            <div className={`file-selector-row ${this.props.selected ? 'selected' : ''}`}
                onClick={this.props.onClick}>
                {this.props.fileName}
            </div >
        )
    }
}

class FileSelectorButton extends React.Component<{
    onClick: () => void,
    disabled: boolean
}> {
    render() {
        return (
            <div className='file-selector-button'>
                <button
                    // className={this.props.disabled ? 'button-disabled' : ''}
                    disabled={this.props.disabled}
                    onClick={this.props.onClick}>
                    Select
                </button>
            </div>
        )
    }
}