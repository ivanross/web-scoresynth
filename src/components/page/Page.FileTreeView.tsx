import * as React from 'react'
import { FileLoader, FileTreeItem } from '../tree-view-file';
import { WorkspaceFile } from '../../logic/workspace';
import { ContextMenuPosition } from '../menu';
import { FileTreeViewContextMenu } from '../context-menu';
export { FileTreeView }

interface Props {
    style: FixedDivStyle,
    titleHeight: number,

    allFiles: WorkspaceFile[],
    deleteFile: (file: WorkspaceFile) => void,
    renameFile: (file: WorkspaceFile) => void,
    createFileFromContent: (fileName: string, content: string) => void,
    downloadFile: (file: WorkspaceFile) => void
}


interface State {
    contextMenuPosition: ContextMenuPosition
}

class FileTreeView extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { contextMenuPosition: null }
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }

    onContextMenu(e: React.MouseEvent<HTMLDivElement>) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }
    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    render() {
        const { style, titleHeight, allFiles } = this.props
        const { contextMenuPosition } = this.state


        return (
            <div className='tree-view' style={style}
                onContextMenu={this.onContextMenu}>
                <div className='tree-view-header' style={{ height: titleHeight }}>
                    FILES
                </div>

                <div className='tree-view-content' style={{ height: style.height - titleHeight }}>
                    {allFiles.sort((a, b) => a.fileName.localeCompare(b.fileName)).map((f, i) => {
                        return <FileTreeItem
                            key={i}
                            file={f}
                            fileName={f.fileName}
                            deleteFile={() => this.props.deleteFile(f)}
                            renameFile={() => this.props.renameFile(f)}
                            downloadFile={() => this.props.downloadFile(f)} />
                    })}

                    <FileLoader
                        createFile={this.props.createFileFromContent} />


                </div>
                {contextMenuPosition &&
                    <FileTreeViewContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        closeContextMenu={this.closeContextMenu} />
                }
            </div >
        )
    }
}
