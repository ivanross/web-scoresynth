import * as React from 'react'
import { GraphicPetriNet, GraphicPlace, GraphicTransition, GraphicArc } from '../../logic/graphic';
import { ControlPanelPlace, ControlPanelTransition, ControlPanelArc, ControlPanelPetriNet } from '../control-panel'
import { WorkspaceNet } from '../../logic/workspace';
export { ControlPanel }

interface Props {
    style: FixedDivStyle

    openNet: WorkspaceNet,
    updateGraphicNet: (data: GraphicPetriNet) => void
}

class ControlPanel extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
        this.updateArc = this.updateArc.bind(this)
        this.updatePlace = this.updatePlace.bind(this)
        this.updateTransition = this.updateTransition.bind(this)
        this.updateNet = this.updateNet.bind(this)
    }

    updatePlace(gp: GraphicPlace) {
        let { net } = this.props.openNet;
        let i = net.petriNet.places.indexById(gp.place.id)
        net.petriNet.places.elements[i] = gp.place
        this.props.updateGraphicNet(net)
    }

    updateTransition(gt: GraphicTransition) {
        let { net } = this.props.openNet;
        let i = net.petriNet.places.indexById(gt.transition.id)
        net.petriNet.transitions.elements[i] = gt.transition
        this.props.updateGraphicNet(net)
    }

    updateArc(ga: GraphicArc) {
        let { net } = this.props.openNet;
        let i = net.petriNet.arcs.indexById(ga.arc.id)
        net.petriNet.arcs.elements[i] = ga.arc
        this.props.updateGraphicNet(net)
    }

    updateNet(gpn: GraphicPetriNet) {
        this.props.updateGraphicNet(gpn)
    }

    render() {
        const { style, openNet } = this.props
        let panel: any = null
        if (openNet) {
            let element = openNet.selectedElement
            if (element instanceof GraphicPlace)
                panel = <ControlPanelPlace
                    el={element}
                    net={openNet.net}
                    updatePlace={this.updatePlace}
                    updateNet={this.updateNet}
                />
            else if (element instanceof GraphicTransition)
                panel = <ControlPanelTransition
                    el={element}
                    net={openNet.net}
                    updateTransition={this.updateTransition}
                    updateNet={this.updateNet}
                />
            else if (element instanceof GraphicArc)
                panel = <ControlPanelArc
                    el={element}
                    updateArc={this.updateArc}
                />
            else
                panel = <ControlPanelPetriNet
                    net={this.props.openNet.net} />
        }


        return (
            <div id="control-panel"
                style={style}>
                {panel}
            </div>
        )
    }
}

