import * as React from 'react'
import { PNAction } from '../../logic/graphic';
import { MenuSeparator, MenuItem, MenuBarItem } from '../menu';

interface Props {
    style: FixedDivStyle,

    createNet: () => void,
    saveNet: () => void,
    saveAllNets: () => void,
    saveAs: () => void,
    closeCurrentNet: () => void,
    closeAllNets: () => void,
    closeOthersNets: () => void,
    closeSaved: () => void,
    deleteCurrentNet: () => void,
    renameCurrentNet: () => void,
    setAction: (action: PNAction) => void,
    action: PNAction,
    thereIsAtLeastOneNetOpen: boolean,
    downloadAll: () => void
}

export class MenuBar extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props)
    }

    render() {
        const { style } = this.props
        return (
            <div id='menu-bar' style={style}>
                <MenuBarItem name="File">
                    <MenuItem name="New" onClick={this.props.createNet} />

                    <MenuSeparator />

                    <MenuItem name="Save"
                        onClick={this.props.saveNet}
                        shortcut={'Ctrl/Cmd+S'}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />

                    <MenuItem name="Save all" onClick={this.props.saveAllNets}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />
                    
                    <MenuItem name="Save as" onClick={this.props.saveAs}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />

                    

                    <MenuSeparator />

                    <MenuItem name="Close"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.closeCurrentNet}
                    />

                    <MenuItem name="Close all"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.closeAllNets}
                    />

                    <MenuItem name="Close others"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.closeOthersNets}
                    />

                    <MenuItem name="Close saved"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.closeSaved}
                    />

                    <MenuSeparator />

                    <MenuItem name="Delete"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.deleteCurrentNet}
                    />

                    <MenuSeparator />

                    <MenuItem name="Rename"
                        disabled={!this.props.thereIsAtLeastOneNetOpen}
                        onClick={this.props.renameCurrentNet}
                    />

                    <MenuSeparator />
                    <MenuItem name='Download project' onClick={this.props.downloadAll}/>

                </MenuBarItem>
                <MenuBarItem name="Edit">
                    <MenuItem name="Select"
                        selected={this.props.action == PNAction.Select}
                        onClick={() => this.props.setAction(PNAction.Select)} />
                    <MenuItem name="Erase"
                        selected={this.props.action == PNAction.Eraser}
                        onClick={() => this.props.setAction(PNAction.Eraser)}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />

                    <MenuSeparator />

                    <MenuItem name="Draw Place"
                        selected={this.props.action == PNAction.AddPlace}
                        onClick={() => this.props.setAction(PNAction.AddPlace)}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />
                    <MenuItem name="Draw Transition V"
                        selected={this.props.action == PNAction.AddVerticalTransition}
                        onClick={() => this.props.setAction(PNAction.AddVerticalTransition)}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />
                    <MenuItem name="Draw Transition H"
                        selected={this.props.action == PNAction.AddHorizontalTransition}
                        onClick={() => this.props.setAction(PNAction.AddHorizontalTransition)}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />
                    <MenuItem name="Draw Arc"
                        selected={this.props.action == PNAction.AddArc}
                        onClick={() => this.props.setAction(PNAction.AddArc)}
                        disabled={!this.props.thereIsAtLeastOneNetOpen} />
                </MenuBarItem>
            </div>
        )
    }
}
