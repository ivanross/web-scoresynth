import * as React from 'react'
export { EmptyCanvas }

interface Props {
    style: FixedDivStyle
}

function EmptyCanvas(props: Props) {
    return <div 
        id="empty-canvas"
        style={props.style} />
}