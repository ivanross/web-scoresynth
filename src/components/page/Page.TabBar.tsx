import * as React from 'react'
import { WorkspaceNet } from '../../logic/workspace';
import { Tab } from '../tab-bar'
import { ContextMenuPosition } from '../menu';
import { TabBarContextMenu } from '../context-menu';
export { TabBar }

interface Props {
    style: FixedDivStyle
    nets: WorkspaceNet[],
    activeNet: WorkspaceNet,
    setSelectedNet: (openNet: WorkspaceNet) => void,
    closeNet: (net: WorkspaceNet) => void,
    closeAll: () => void,
    closeOthers: (net: WorkspaceNet) => void,
    saveNet: (net: WorkspaceNet) => void,
    saveAs: (net: WorkspaceNet) => void,
    thereIsAtLeastOneNetOpen: boolean
}

interface State {
    contextMenuPosition: ContextMenuPosition
}

class TabBar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { contextMenuPosition: null }
        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)
    }

    onContextMenu(e: any) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }


    render() {
        const { style, nets, activeNet } = this.props
        const { contextMenuPosition } = this.state
        return (
            <div id="tab-bar" style={style}
                onContextMenu={this.onContextMenu}>
                {nets.map((n, i) => {
                    return <Tab
                        key={i}
                        name={n.fileName}
                        net={n}
                        active={n == activeNet}
                        saved={n.saved}
                        setSelectedNet={this.props.setSelectedNet}
                        saveNet={() => this.props.saveNet(n)}
                        saveAs={() => this.props.saveAs(n)}
                        closeAll={this.props.closeAll}
                        closeOthers={() => this.props.closeOthers(n)}
                        closeNet={() => this.props.closeNet(n)} />
                })}
                {contextMenuPosition &&
                    <TabBarContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        closeAll={this.props.closeAll}
                        thereIsAtLeastOneNetOpen={this.props.thereIsAtLeastOneNetOpen}
                        closeContextMenu={this.closeContextMenu} />
                }
            </div>
        )
    }
}
