import * as React from "react";
import * as $ from 'jquery'
import { Transition, Place, ArcType, Arc } from '../../logic/petri'
import { GraphicTransition, GraphicPlace, GraphicArc, GraphicPetriNet, PNAction, DrawingArcInfo } from '../../logic/graphic'
import { WorkspaceNet } from "../../logic/workspace";
import { Temp, CanvTransition, CanvPlace, CanvArc } from "../canvas";
import { ContextMenuPosition } from "../menu";
import { CanvasContextMenu } from "../context-menu";
const d3 = require('d3')

export { Canvas }

const { max, min, round } = Math

interface Props {
    openNet: WorkspaceNet,
    updateWorkspaceNet: (data: WorkspaceNet) => void,
    updateGraphicNet: (data: GraphicPetriNet) => void,
    style: FixedDivStyle,
    action: PNAction,
    setAction: (action: PNAction) => void,
    saveNet: () => void
}

interface State {
    arcReceivedFlag: boolean,
    mouse: Coord,
    drawArc: DrawingArcInfo,
    rel: Coord,
    dragging: boolean,
    contextMenuPosition: ContextMenuPosition
}

class Canvas extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            rel: null,
            arcReceivedFlag: false,
            mouse: null,
            drawArc: null,
            dragging: false,
            contextMenuPosition: null
        }
        this.updateGraphicArc = this.updateGraphicArc.bind(this)
        this.updateGraphicPlace = this.updateGraphicPlace.bind(this)
        this.updateGraphicTransition = this.updateGraphicTransition.bind(this)
        this.updateSelected = this.updateSelected.bind(this)
        this.updateTransform = this.updateTransform.bind(this)

        this.createArc = this.createArc.bind(this)
        this.createPlace = this.createPlace.bind(this)
        this.createTransition = this.createTransition.bind(this)
        this.delete = this.delete.bind(this)

        this.receiveSelected = this.receiveSelected.bind(this)
        this.receiveArcSelected = this.receiveArcSelected.bind(this)
        this.setDrawingArcPoint = this.setDrawingArcPoint.bind(this)

        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseMoveWhileDrawing = this.onMouseMoveWhileDrawing.bind(this)
        this.onMouseMoveWhileDrag = this.onMouseMoveWhileDrag.bind(this)
        this.onMouseUpWhileDrag = this.onMouseUpWhileDrag.bind(this)
        this.onScroll = this.onScroll.bind(this)

        this.onContextMenu = this.onContextMenu.bind(this)
        this.closeContextMenu = this.closeContextMenu.bind(this)

        this.updateIsInputPlace = this.updateIsInputPlace.bind(this)
        this.updateIsOutputPlace = this.updateIsOutputPlace.bind(this)
        this.updateIsInputTransition = this.updateIsInputTransition.bind(this)
        this.updateIsOutputTransition = this.updateIsOutputTransition.bind(this)
    }

    componentDidMount() {
        $(`#canvas-svg`).mousedown(this.onMouseDown)
        document.getElementById('canvas').addEventListener('wheel', this.onScroll)
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        // Handle Dragging Event
        if (this.state.dragging && !prevState.dragging) {
            $(document)
                .mousemove(this.onMouseMoveWhileDrag)
                .mouseup(this.onMouseUpWhileDrag)
        } else if (!this.state.dragging && prevState.dragging) {
            $(document).unbind('mousemove').unbind('mouseup')
        }

        // Handle drawArc info when changing net
        if (prevProps.openNet != this.props.openNet) {
            this.setState({ drawArc: null })
        }

        // Handle Drawing Event
        let prewDraw = (prevProps.action != PNAction.Select) && (prevProps.action != PNAction.Eraser)
        let currDraw = (this.props.action != PNAction.Select) && (this.props.action != PNAction.Eraser)
        if (!prewDraw && currDraw) {
            $(window).bind('mousemove', this.onMouseMoveWhileDrawing);
        } else if (prewDraw && !currDraw) {
            $(window).unbind('mousemove', this.onMouseMoveWhileDrawing);
        }
    }


    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        if (nextProps.action != PNAction.AddArc) prevState.drawArc = null;
        return prevState
    }

    onMouseMoveWhileDrag(e: any) {
        if (!this.state.dragging) return
        const { x, y, scale } = this.props.openNet.transform
        let { rel } = this.state
        this.updateTransform({
            scale,
            x: e.pageX - rel.x,
            y: e.pageY - rel.y
        })
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseUpWhileDrag(e: any) {
        this.setState({ dragging: false })
        e.stopPropagation()
        e.preventDefault()
    }

    onScroll(e: any) {
        let { x, y, scale } = this.props.openNet.transform
        let newScale = +max(.5, min(1.5, scale - e.deltaY * .001)).toFixed(4)
        this.updateTransform({
            scale: newScale,
            x,
            y
        })
    }

    onMouseDown(e: any) {
        // only left mouse button
        if (e.button !== 0) return;

        const { mouse } = this.state
        const { scale, x, y } = this.props.openNet.transform

        switch (this.props.action) {
            case PNAction.Select:
                this.receiveSelected(null)
                this.setState({
                    dragging: true,
                    rel: {
                        x: e.pageX - x,
                        y: e.pageY - y
                    }
                })
                break;
            case PNAction.AddArc:
                this.setDrawingArcPoint(mouse)
                break;
            case PNAction.AddPlace:
                this.createPlace(mouse)
                break;
            case PNAction.AddVerticalTransition:
                this.createTransition(mouse, false)
                break;
            case PNAction.AddHorizontalTransition:
                this.createTransition(mouse, true);
                break;
            default:
                break;
        }
        e.stopPropagation()
        e.preventDefault()
    }

    onMouseMoveWhileDrawing(e: any) {
        const { top, left } = this.props.style
        const { scale, x, y } = this.props.openNet.transform
        this.setState({
            mouse: {
                x: (+e.pageX - left - x) / scale,
                y: (+e.pageY - top - y) / scale
            }
        })

    }


    createPlace(c: Coord) {
        let { net } = this.props.openNet
        net.petriNet.places.create(c)
        this.props.updateGraphicNet(net)
    }

    createTransition(c: Coord, horizontal: boolean) {
        let { net } = this.props.openNet
        net.petriNet.transitions.create(c, horizontal)
        this.props.updateGraphicNet(net)
    }

    createArc(i: DrawingArcInfo) {
        let { net } = this.props.openNet
        net.petriNet.arcs.add(i.place, i.transition, i.type, i.points)
        this.props.updateGraphicNet(net)
    }



    delete(el: GraphicArc | GraphicPlace | GraphicTransition) {
        let { net } = this.props.openNet
        if (el instanceof GraphicPlace) {
            net.petriNet.removePlace(el.place)
        } else if (el instanceof GraphicTransition) {
            net.petriNet.removeTransition(el.transition)
        } else {
            net.petriNet.removeArc(el.arc)
        }
        this.props.updateGraphicNet(net)
    }



    // When an Arc is clicked (MouseDown on <path>), 
    // the background (Canvas) is selected immediately afterwards, so the arc 
    // is never focused. With a flag, the arrow can be selected

    receiveArcSelected(ga: GraphicArc) {
        if (this.props.action == PNAction.Eraser)
            this.delete(ga)
        else {
            this.setState({ arcReceivedFlag: true })
            this.updateSelected(ga)
        }

    }

    receiveSelected(e: any) {

        if (this.state.arcReceivedFlag) {
            this.setState({ arcReceivedFlag: false })
        } else if (this.props.action == PNAction.Eraser) {
            this.delete(e)
            this.updateSelected(null)
        } else if (this.props.action == PNAction.AddArc) {

            // Handle drawing arc (Place or Transition clicked)
            let { drawArc } = this.state

            if (!drawArc) {

                if (e instanceof GraphicPlace) {
                    drawArc = {
                        place: e.place,
                        type: ArcType.FromPlaceToTransition,
                        points: [],
                        transition: null
                    }
                    this.setState({ drawArc })
                } else if (e instanceof GraphicTransition) {
                    drawArc = {
                        place: null,
                        type: ArcType.FromTransitionToPlace,
                        points: [],
                        transition: e.transition
                    }
                    this.setState({ drawArc })
                }

            } else {
                if (e instanceof GraphicPlace && drawArc.transition) {
                    drawArc.place = e.place
                } else if (e instanceof GraphicTransition && drawArc.place) {
                    drawArc.transition = e.transition
                } else return
                this.createArc(drawArc)
                this.setState({ drawArc: null })

            }


        } else {
            this.updateSelected(e)
        }
    }

    setDrawingArcPoint(c: Coord) {
        let { drawArc } = this.state
        if (drawArc) {
            drawArc.points.push([c.x, c.y])
        }
    }

    //////////////////////////
    ////                 /////
    //// UPDATE OPEN NET /////
    ////                 /////
    //////////////////////////

    // Sets the selected element in OpenNet
    updateSelected(el: GraphicArc | GraphicPlace | GraphicTransition | null) {
        let { openNet } = this.props
        openNet.selectedElement = el
        this.props.updateWorkspaceNet(openNet)
    }

    // Sets the transform info in OpenNet
    updateTransform(tr: TransformData) {
        let { openNet } = this.props
        openNet.transform = tr
        this.props.updateWorkspaceNet(openNet)
    }

    updateGraphicPlace(i: number) {
        return (gp: GraphicPlace) => {
            let { net } = this.props.openNet
            net.petriNet.places.elements[i] = gp.place
            this.props.updateGraphicNet(net)
        }
    }

    updateGraphicTransition(i: number) {
        return (gt: GraphicTransition) => {
            let { net } = this.props.openNet
            net.petriNet.transitions.elements[i] = gt.transition
            this.props.updateGraphicNet(net)
        }
    }

    updateGraphicArc(i: number) {
        return (ga: GraphicArc) => {
            let { net } = this.props.openNet
            net.petriNet.arcs.elements[i] = ga.arc
            this.props.updateGraphicNet(net)
        }
    }

    // Contex Menu Functions
    onContextMenu(e: any) {
        e.preventDefault()
        e.stopPropagation()
        this.setState({
            contextMenuPosition: {
                left: e.pageX,
                top: e.pageY
            }
        })
    }

    closeContextMenu() {
        this.setState({ contextMenuPosition: null })
    }

    updateIsInputPlace(p: Place) {
        return (is: boolean) => {
            const { net } = this.props.openNet
            net.petriNet.updateIsInput(
                p.graphicPlace,
                is
            )
            this.props.updateGraphicNet(net)
        }
    }
    updateIsOutputPlace(p: Place) {
        return (is: boolean) => {
            const { net } = this.props.openNet
            net.petriNet.updateIsOutput(
                p.graphicPlace,
                is
            )
            this.props.updateGraphicNet(net)
        }
    }
    updateIsInputTransition(t: Transition) {
        return (is: boolean) => {
            const { net } = this.props.openNet
            net.petriNet.updateIsInput(
                t.graphicTransition,
                is
            )
            this.props.updateGraphicNet(net)
        }
    }
    updateIsOutputTransition(t: Transition) {
        return (is: boolean) => {
            const { net } = this.props.openNet
            net.petriNet.updateIsOutput(
                t.graphicTransition,
                is
            )
            this.props.updateGraphicNet(net)
        }
    }
    

    render() {
        const { transitions, places, arcs } = this.props.openNet.net.petriNet
        const { style, action } = this.props
        const { drawArc, mouse, contextMenuPosition } = this.state
        const { x, y, scale } = this.props.openNet.transform
        const elementSelected = this.props.openNet.selectedElement

        let isArcSelectable = (this.props.action == PNAction.Select) || (this.props.action == PNAction.Eraser)
        let isNodeSelectable = isArcSelectable || (this.props.action == PNAction.AddArc)

        const t = transitions.elements.map((t, i) => {
            let isSelected = false
            if (elementSelected instanceof GraphicTransition)
                isSelected = elementSelected.transition.id == t.id
            return (
                <CanvTransition
                    key={`graphictransition-${i}`}
                    gt={t.graphicTransition}
                    updateData={this.updateGraphicTransition(i)}
                    sendSelected={this.receiveSelected}
                    transform={this.props.openNet.transform}
                    isSelected={isSelected}
                    selectable={isNodeSelectable}
                    updateIsInput={this.updateIsInputTransition(t)}
                    updateIsOutput={this.updateIsOutputTransition(t)}
                    delete={() => this.delete(t.graphicTransition)} />
            )
        })
        const p = places.elements.map((p, i) => {
            let isSelected = false
            if (elementSelected instanceof GraphicPlace)
                isSelected = elementSelected.place.id == p.id
            return (
                <CanvPlace
                    key={`graphicplace-${i}`}
                    gp={p.graphicPlace}
                    updateData={this.updateGraphicPlace(i)}
                    sendSelected={this.receiveSelected}
                    transform={this.props.openNet.transform}
                    isSelected={isSelected}
                    selectable={isNodeSelectable}
                    updateIsInput={this.updateIsInputPlace(p)}
                    updateIsOutput={this.updateIsOutputPlace(p)}
                    delete={() => this.delete(p.graphicPlace)} />
            )
        })
        const a = arcs.elements.map((a, i) => {
            let isSelected = false
            if (elementSelected instanceof GraphicArc)
                isSelected = elementSelected.arc.id == a.id
            return (
                <CanvArc
                    key={`graphicarc-${i}`}
                    ga={a.graphicArc}
                    transform={this.props.openNet.transform}
                    updateData={this.updateGraphicArc(i)}
                    sendSelected={this.receiveArcSelected}
                    isSelected={isSelected}
                    selectable={isArcSelectable} />
            )
        })

        let buildArc = null
        if (action == PNAction.AddArc && drawArc) {
            let first: [number, number]
            if (drawArc.type == ArcType.FromPlaceToTransition)
                first = [
                    drawArc.place.graphicPlace.x,
                    drawArc.place.graphicPlace.y
                ]
            else {
                first = [
                    drawArc.transition.graphicTransition.x,
                    drawArc.transition.graphicTransition.y
                ]
            }
            let pointsAll = [first]
                .concat(drawArc.points)
                .concat([[mouse.x, mouse.y]])
            buildArc = <path
                d={d3.line()(pointsAll)}
                fill="none"
                stroke={GraphicArc.defaults.stroke}
                strokeWidth={GraphicArc.defaults.strokeWidth}
                opacity={.25} />
        }

        return (
            <div id="canvas"
                style={style}
                onContextMenu={this.onContextMenu} >
                <svg id='canvas-svg'
                    width={style.width}
                    height={style.height}
                >
                    <g id="content-group" transform={`translate(${x},${y}) scale(${scale})`}>
                        <g id="arcs-group">
                            {a}
                            {buildArc}
                        </g>
                        <g id="nodes-group">
                            {t}
                            {p}
                            {this.props.action == PNAction.AddPlace && this.state.mouse &&
                                <Temp.Circle mouse={this.state.mouse} />
                            }
                            {this.props.action == PNAction.AddVerticalTransition && this.state.mouse &&
                                <Temp.RectVertical mouse={this.state.mouse} />
                            }
                            {this.props.action == PNAction.AddHorizontalTransition && this.state.mouse &&
                                <Temp.RectHorizontal mouse={this.state.mouse} />
                            }
                        </g>
                        <g id="labels-group" />
                    </g>
                </svg>

                {contextMenuPosition != null &&
                    <CanvasContextMenu
                        position={contextMenuPosition}
                        onClickOut={this.closeContextMenu}
                        action={action}
                        setAction={this.props.setAction}
                        saveNet={this.props.saveNet}
                        closeContextMenu={this.closeContextMenu} />
                }

            </div>
        );
    }
}

