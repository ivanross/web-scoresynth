import * as React from 'react'
import { GraphicArc } from "../../logic/graphic";
import { CPArcPoint, CPArcTension } from './arc'
import { CPNumber, CPColor, CPSelect } from './shared';

export { ControlPanelArc }
interface Props {
    el: GraphicArc,
    updateArc: (ga: GraphicArc) => void
}

interface State {
    tensionValDisplayed: number
}

class ControlPanelArc extends React.Component<Props, State>{
    constructor(props: Props) {
        super(props);

        this.state = { tensionValDisplayed: props.el.tension }
        this.updateTokensWeight = this.updateTokensWeight.bind(this)
        this.updateProbWeight = this.updateProbWeight.bind(this)
        this.updateTension = this.updateTension.bind(this)
        this.updateStrokeWidth = this.updateStrokeWidth.bind(this)
        this.updatePoint = this.updatePoint.bind(this)
        this.updateColor = this.updateColor.bind(this)

        this.destroyPoint = this.destroyPoint.bind(this)
    }

    updateTokensWeight(tw: number) {
        let { el } = this.props
        el.arc.tokensWeight = tw
        this.props.updateArc(el)
    }

    updateProbWeight(pw: number) {
        let { el } = this.props
        el.arc.probWeight = pw
        this.props.updateArc(el)
    }

    updateStrokeWidth(e: number) {
        let { el } = this.props
        el.strokeWidth = e
        this.props.updateArc(el)
    }

    updateTension(e: number) {
        let { el } = this.props
        el.tension = e
        this.props.updateArc(el)
    }

    updatePoint(newPoint: [number, number], index: number) {
        let { el } = this.props
        el.pointsMiddle[index] = newPoint
        this.props.updateArc(el)
    }

    updateColor(c: string) {
        let { el } = this.props
        el.stroke = c
        this.props.updateArc(el)
    }

    destroyPoint(i: number) {
        let { el } = this.props
        let old = el.pointsMiddle
        el.pointsMiddle.splice(i, 1)
        this.props.updateArc(el)
    }

    render() {
        const { el } = this.props

        const tokensWeight = <CPNumber
            title="Tokens Weight"
            value={el.arc.tokensWeight}
            step={1}
            updateData={this.updateTokensWeight} />

        const probWeight = <CPNumber
            title="Prob Weight"
            value={el.arc.probWeight}
            step={1}
            updateData={this.updateProbWeight} />

        const strokeWidth = <CPNumber
            title="Stroke Width"
            value={el.strokeWidth}
            step={1}
            updateData={this.updateStrokeWidth} />

        const color = <CPColor
            title="Color"
            value={el.stroke}
            updateData={this.updateColor} />

        const tension = (
            <CPArcTension
                tension={el.tension}
                updateData={this.updateTension} />
        )

        const points = el.pointsMiddle.length == 0
            ? null
            : (
                <div className="cp-comp-arcpoints">
                    <table>
                        <thead>
                            <tr>
                                <td />
                                <td>X</td>
                                <td>Y</td>
                                <td />
                            </tr>
                        </thead>
                        <tbody>
                            {
                                el.pointsMiddle.map((p, i) => {
                                    return (
                                        <CPArcPoint key={i}
                                            index={i}
                                            point={p}
                                            destroy={this.destroyPoint}
                                            updateData={this.updatePoint}
                                        />
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            )

        return (
            <div className="cp-content">
                <div className="cp-block">
                    <div className="cp-header">
                        Arc
                    </div>
                </div>

                <div className="cp-block">
                    <div className="cp-title">
                        Element Attributes
                    </div>
                    {tokensWeight}
                    {probWeight}
                </div>

                <div className="cp-block">
                    <div className="cp-title">
                        Graphics
                    </div>
                    {/* TODO: Font Control */}
                    {strokeWidth}
                    {color}
                    {tension}
                    {points}
                </div>



            </div>
        )
    }
}

