import * as React from 'react'
import { GraphicTransition, GraphicPetriNet } from "../../logic/graphic";
import { CPText, CPColor, CPPosition, CPNumber, CPSelect, CPCheckbox, CPSelectFile } from './shared'
import { TransitionType } from '../../logic/petri';
export { ControlPanelTransition }
interface Props {
    el: GraphicTransition,
    net: GraphicPetriNet, // needed to set isInput/isOutput
    updateTransition: (gt: GraphicTransition) => void,
    updateNet: (gpn: GraphicPetriNet) => void
}
class ControlPanelTransition extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props)
        this.updateName = this.updateName.bind(this)
        this.updateWidth = this.updateWidth.bind(this)
        this.updateHeight = this.updateHeight.bind(this)
        this.updateOrientation = this.updateOrientation.bind(this)
        this.updateFillColor = this.updateFillColor.bind(this)
        this.updateStrokeWidth = this.updateStrokeWidth.bind(this)
        this.updateStrokeColor = this.updateStrokeColor.bind(this)
        this.updatePosition = this.updatePosition.bind(this)
        this.updateVtuScale = this.updateVtuScale.bind(this)
        this.updateIsInput = this.updateIsInput.bind(this)
        this.updateIsOutput = this.updateIsOutput.bind(this)
        this.updateFileName = this.updateFileName.bind(this)

    }

    updateName(name: string) {
        let { el } = this.props
        el.transition.name = name
        this.props.updateTransition(el)
    }

    updateWidth(w: number) {
        let { el } = this.props
        el.width = w
        this.props.updateTransition(el)
    }

    updateHeight(h: number) {
        let { el } = this.props
        el.height = h
        this.props.updateTransition(el)
    }

    updateOrientation(isVertical: number) {
        let { el } = this.props
        el.isHorizontal = isVertical == 1
        this.props.updateTransition(el)
    }

    updateFillColor(c: string) {
        let { el } = this.props
        el.fill = c
        this.props.updateTransition(el)
    }

    updateStrokeColor(c: string) {
        let { el } = this.props
        el.stroke = c
        this.props.updateTransition(el)
    }

    updateStrokeWidth(sw: number) {
        let { el } = this.props
        el.strokeWidth = sw
        this.props.updateTransition(el)
    }

    updatePosition(x: number, y: number) {
        let { el } = this.props
        el.x = x
        el.y = y
        this.props.updateTransition(el)
    }

    updateVtuScale(vtuScale: number) {
        let { el } = this.props
        el.transition.vtuScale = vtuScale
        this.props.updateTransition(el)
    }

    updateIsInput(is: boolean) {
        let { el, net } = this.props
        net.petriNet.updateIsInput(el, is)
        this.props.updateNet(net)
    }

    updateIsOutput(is: boolean) {
        let { el, net } = this.props
        net.petriNet.updateIsOutput(el, is)
        this.props.updateNet(net)
    }

    updateFileName(fileName: string) {
        const { el } = this.props
        el.transition.file = fileName
        this.props.updateTransition(el)
    }


    render() {
        const { el } = this.props

        return (
            <div className="cp-content">

                <div className="cp-block">
                    <div className="cp-header">
                        Transition
                    </div>
                </div>

                <div className="cp-block">
                    <div className="cp-title">
                        Element Attributes
                    </div>

                    <CPText
                        title="Name"
                        value={el.transition.name}
                        updateData={this.updateName} />
                </div>

                <div className="cp-block">
                    <div className="cp-title">
                        Graphics
                    </div>
                    <CPNumber
                        title="Width"
                        value={el.width}
                        step={1}
                        updateData={this.updateWidth} />

                    <CPNumber
                        title="Height"
                        value={el.height}
                        step={1}
                        updateData={this.updateHeight} />

                    <CPSelect
                        title="Orientation"
                        options={['Vertical', 'Horizional']}
                        value={el.isHorizontal ? 1 : 0}
                        updateData={this.updateOrientation} />

                    <CPColor
                        title="Fill Color"
                        value={el.fill}
                        updateData={this.updateFillColor} />

                    <CPNumber
                        title="Stroke Width"
                        value={el.strokeWidth}
                        step={1}
                        updateData={this.updateStrokeWidth} />

                    <CPColor
                        title="Stroke Color"
                        value={el.stroke}
                        updateData={this.updateStrokeColor} />

                    <CPPosition
                        x={el.x}
                        y={el.y}
                        updateData={this.updatePosition} />

                    {/* TODO: Font Control */}

                </div>


                <div className="cp-block">
                    <div className="cp-title">
                        Misc
                    </div>

                    {/* TODO: Vtu Ammounts Control */}

                    <CPSelectFile
                        title='File'
                        value={el.transition.file}
                        updateData={this.updateFileName}
                    />


                    <CPText
                        title='Type'
                        value={TransitionType[el.transition.type]}
                        disabled={true}
                        updateData={null} />

                    <CPNumber
                        title="Vtu Scale"
                        value={el.transition.vtuScale}
                        step={1}
                        updateData={this.updateVtuScale} />

                    <CPCheckbox
                        title={"Input Node"}
                        value={el.transition.isInput}
                        updateData={this.updateIsInput} />

                    <CPCheckbox
                        title={"Output Node"}
                        value={el.transition.isOutput}
                        updateData={this.updateIsOutput} />

                </div>






            </div>
        )
    }
}