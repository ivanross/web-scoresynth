import * as React from 'react'
export { CPArcTension }

interface Props {
    tension: number,
    updateData: (newTension: number) => void
}
interface State {
    displayedTension: number
}

class CPArcTension extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = { displayedTension: props.tension }
        this.onSliderChange = this.onSliderChange.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
        this.onInputSubmit = this.onInputSubmit.bind(this)
    }
    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        prevState.displayedTension = nextProps.tension
        return prevState
    }
    onSliderChange(e: any) {
        this.props.updateData(+e.target.value)
    }
    onInputChange(e: any) {
        this.setState({ displayedTension: +e.target.value })
    }
    onInputSubmit(e: any) {
        this.props.updateData(this.state.displayedTension)
        e.preventDefault()
    }

    render() {
        const { tension } = this.props
        const { displayedTension } = this.state
        return (
            <div className="cp-comp-tension cp-comp">
                <form className="cp-comp-tension-row" onSubmit={this.onInputSubmit}>
                    <div className="cp-comp-title">
                        Tension
                    </div>
                    <div className="cp-comp-input">
                        <input
                            type="number"
                            min={0}
                            max={1}
                            width={100}
                            step={.01}
                            value={displayedTension}
                            onChange={this.onInputChange} />
                    </div>
                </form>
                <div className="cp-comp-tension-row">
                    <div className="cp-comp-title" />
                    <div className="cp-comp-input">
                        <input
                            type="range"
                            min={0}
                            max={1}
                            step={.01}
                            value={tension}
                            onChange={this.onSliderChange} />
                    </div>
                </div>
            </div>
        )
    }
}