import * as React from 'react'
import * as $ from 'jquery'
export { CPArcPoint }

interface Props {
    index: number,
    point: [number, number],
    destroy: (index: number) => void
    updateData: (newPoint: [number, number], index: number) => void
}
interface State {
    x: number,
    y: number
}

class CPArcPoint extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            x: props.point[0],
            y: props.point[1]
        }
        this.onButtonClick = this.onButtonClick.bind(this)
        this.onXInputChange = this.onXInputChange.bind(this)
        this.onYInputChange = this.onYInputChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        return {
            x: nextProps.point[0],
            y: nextProps.point[1]
        }
    }

    onButtonClick(e: any) {
        this.props.destroy(this.props.index)
        e.preventDefault()
    }

    onXInputChange(e: any) {
        this.setState({ x: +e.target.value })
    }

    onYInputChange(e: any) {
        this.setState({ y: +e.target.value })
    }
    onSubmit(e: any) {
        e.preventDefault()
        const { updateData, index } = this.props
        const { x, y } = this.state
        updateData([x, y], index)

    }

    render() {
        const { index, point } = this.props
        const { x, y } = this.state
        return (
            <tr>
                <td className="cp-comp-ap-name">P{index + 1}</td>
                <td className="cp-comp-ap-x">
                    <form onSubmit={this.onSubmit}>
                        <input
                            type="number"
                            name='x'
                            value={x}
                            onChange={this.onXInputChange}
                        />
                    </form>
                </td>
                <td className="cp-comp-ap-y">
                    <form onSubmit={this.onSubmit}>
                        <input
                            type="number"
                            name='y'
                            value={y}
                            onChange={this.onYInputChange} />
                        <input type="submit" hidden />
                    </form>
                </td>
                <td className="cp-comp-ap-canc">
                    {/* <button onClick={this.onButtonClick}>X</button> */}
                    <i className="fas fa-times" onClick={this.onButtonClick} />
                </td>
            </tr>
        )
    }
}