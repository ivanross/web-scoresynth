import * as React from "react"
import { GraphicPlace, GraphicPetriNet } from "../../logic/graphic";
import { CPPosition, CPText, CPNumber, CPColor, CPCheckbox } from './shared'
import { PlaceType } from "../../logic/petri";
import { CPSelectFile } from "./shared/CP.SelectFile";
export { ControlPanelPlace }
interface Props {
    el: GraphicPlace,
    net: GraphicPetriNet, // needed to set isInput/isOutput
    updatePlace: (gp: GraphicPlace) => void,
    updateNet: (gpn: GraphicPetriNet) => void
}

class ControlPanelPlace extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props)
        this.updateName = this.updateName.bind(this)
        this.updateCapacity = this.updateCapacity.bind(this)
        this.updateTokens = this.updateTokens.bind(this)
        this.updatePosition = this.updatePosition.bind(this)
        this.updateRadius = this.updateRadius.bind(this)
        this.updateFillColor = this.updateFillColor.bind(this)
        this.updateStrokeColor = this.updateStrokeColor.bind(this)
        this.updateStrokeWidth = this.updateStrokeWidth.bind(this)
        this.updateVtuScale = this.updateVtuScale.bind(this)
        this.updateIsInput = this.updateIsInput.bind(this)
        this.updateIsOutput = this.updateIsOutput.bind(this)
        this.updateFileName = this.updateFileName.bind(this)
    }

    updateName(name: string) {
        let { el } = this.props
        el.place.name = name
        this.props.updatePlace(el)
    }

    updateCapacity(capacity: number) {
        let { el } = this.props
        el.place.capacity = capacity
        this.props.updatePlace(el)
    }

    updateTokens(nTokens: number) {
        let { el } = this.props
        el.place.nTokens = nTokens
        this.props.updatePlace(el)
    }

    updateRadius(radius: number) {
        let { el } = this.props
        el.radius = radius
        this.props.updatePlace(el)
    }

    updateStrokeWidth(sw: number) {
        let { el } = this.props
        el.strokeWidth = sw
        this.props.updatePlace(el)
    }

    updateFillColor(c: string) {
        let { el } = this.props
        el.fill = c
        this.props.updatePlace(el)
    }

    updateStrokeColor(c: string) {
        let { el } = this.props
        el.stroke = c
        this.props.updatePlace(el)
    }

    updatePosition(x: number, y: number) {
        let { el } = this.props
        el.x = x
        el.y = y
        this.props.updatePlace(el)
    }

    updateVtuScale(vtuScale: number) {
        let { el } = this.props
        el.place.vtuScale = vtuScale
        this.props.updatePlace(el)
    }

    updateIsInput(is: boolean) {
        const { el, net } = this.props
        net.petriNet.updateIsInput(el, is)
        this.props.updateNet(net)
    }

    updateIsOutput(is: boolean) {
        const { el, net } = this.props
        net.petriNet.updateIsOutput(el, is)
        this.props.updateNet(net)
    }

    updateFileName(fileName: string) {
        const { el } = this.props
        el.place.file = fileName
        this.props.updatePlace(el)
    }

    render() {
        const { el } = this.props
        return (
            <div className="cp-content">
                <div className="cp-block">
                    <div className="cp-header">
                        Place
                    </div>
                </div>

                <div className="cp-block">
                    <div className="cp-title">
                        Element Attributes
                    </div>
                    <CPText
                        title="Name"
                        value={el.place.name}
                        updateData={this.updateName} />
                    {el.place.type !== PlaceType.Subnet && [
                        <CPNumber
                            key='Tokens'
                            title="Tokens"
                            value={el.place.nTokens}
                            step={1}
                            updateData={this.updateTokens} />
                        ,
                        <CPNumber
                            key='Capacity'
                            title="Capacity"
                            value={el.place.capacity}
                            step={1}
                            updateData={this.updateCapacity} />
                    ]}

                </div>


                <div className="cp-block">
                    <div className="cp-title">
                        Graphics
                    </div>

                    <CPNumber
                        title="Radius"
                        value={el.radius}
                        step={1}
                        updateData={this.updateRadius} />

                    <CPColor
                        title="Fill Color"
                        value={el.fill}
                        updateData={this.updateFillColor} />

                    <CPNumber
                        title="Stroke Width"
                        value={el.strokeWidth}
                        step={1}
                        updateData={this.updateStrokeWidth} />

                    <CPColor
                        title="Stroke Color"
                        value={el.stroke}
                        updateData={this.updateStrokeColor} />

                    <CPPosition
                        x={el.x}
                        y={el.y}
                        updateData={this.updatePosition} />

                    {/* TODO: Font Control */}
                </div>


                <div className="cp-block">
                    <div className="cp-title">
                        Misc
                    </div>

                    {/* TODO: Vtu Ammounts Control */}



                    <CPSelectFile
                        title='File'
                        value={el.place.file}
                        updateData={this.updateFileName}
                    />

                    <CPText
                        title='Type'
                        value={PlaceType[el.place.type]}
                        disabled={true}
                        updateData={null} />

                    <CPNumber
                        title="Vtu Scale"
                        value={el.place.vtuScale}
                        step={1}
                        updateData={this.updateVtuScale} />

                    <CPCheckbox
                        title={"Input Node"}
                        value={el.place.isInput}
                        updateData={this.updateIsInput} />

                    <CPCheckbox
                        title={"Output Node"}
                        value={el.place.isOutput}
                        updateData={this.updateIsOutput} />
                </div>




            </div>
        )
    }
}