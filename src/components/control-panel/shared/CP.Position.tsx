import * as React from 'react'
export { CPPosition }

interface Props {
    x: number,
    y: number,
    updateData: (x: number, y: number) => void
}
interface State {
    x: number,
    y: number
}

class CPPosition extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            x: props.x,
            y: props.y
        }
        this.onXInputChange = this.onXInputChange.bind(this)
        this.onYInputChange = this.onYInputChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        return {
            x: nextProps.x,
            y: nextProps.y
        }
    }
    onXInputChange(e: any) {
        this.setState({ x: +e.target.value })
    }

    onYInputChange(e: any) {
        this.setState({ y: +e.target.value })
    }

    onSubmit(e: any) {
        e.preventDefault()

        const { updateData } = this.props
        const { x, y } = this.state
        updateData(x, y)

    }
    render() {
        const { x, y } = this.state
        return (
            <React.Fragment>

                <div className="cp-comp-number cp-comp">
                    <form onSubmit={this.onSubmit}>
                        <div className="cp-comp-title">X</div>
                        <div className="cp-comp-input">
                            <input
                                type="number"
                                value={x}
                                onChange={this.onXInputChange} />
                            <input type="submit" hidden />
                        </div>
                    </form>
                </div>
                <div className="cp-comp-number cp-comp">
                    <form onSubmit={this.onSubmit}>
                        <div className="cp-comp-title">Y</div>
                        <div className="cp-comp-input">
                            <input
                                type="number"
                                value={y}
                                onChange={this.onYInputChange} />
                        </div>
                        <input type="submit" hidden />
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

