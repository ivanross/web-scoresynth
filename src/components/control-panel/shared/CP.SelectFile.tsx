import * as React from 'react'
import { FileSelector } from '../../page';

interface Props {
    title: string,
    value: string,
    updateData: (s: string) => void
}

interface State {
    fileSelectorOpen: boolean
}

export class CPSelectFile extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { fileSelectorOpen: false }
        this.openFileSelector = this.openFileSelector.bind(this)
        this.closeFileSelector = this.closeFileSelector.bind(this)
    }

    openFileSelector() { this.setState({ fileSelectorOpen: true }) }
    closeFileSelector() { this.setState({ fileSelectorOpen: false }) }

    render() {
        const { value } = this.props
        const { fileSelectorOpen } = this.state
        return (
            <div className="cp-comp cp-comp-selectfile">

                <div className="cp-comp-title">
                    {this.props.title}
                </div>

                <div className="cp-comp-input">
                    <input
                        type="text"
                        value={value}
                        disabled={true} />
                </div>

                <div className="cp-comp-btns">
                    <i className="fas fa-search" onClick={this.openFileSelector} />
                    <i className="fas fa-times" onClick={() => this.props.updateData('')} />
                </div>

                {fileSelectorOpen &&
                    <FileSelector
                        closeSelector={this.closeFileSelector}
                        sendFileName={(fileName) => {
                            this.closeFileSelector()
                            this.props.updateData(fileName)
                        }} />
                }

            </div>
        )
    }
}