import * as React from 'react'
export { CPCheckbox }

interface Props {
    title: string,
    value: boolean
    updateData: (checked: boolean) => void
}

interface State { }

class CPCheckbox extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onChange = this.onChange.bind(this)
    }
    onChange(e: any) {
        this.props.updateData(e.target.checked)
    }

    render() {
        const { value } = this.props
        return (
            <div className="cp-comp-checkbox cp-comp">
                <form onSubmit={() => { }}>
                    <div className="cp-comp-title">
                        {this.props.title}
                    </div>
                    <div className="cp-comp-input">
                        <input
                            type="checkbox"
                            checked={value}
                            onChange={this.onChange} />
                    </div>
                </form>
            </div>
        )
    }
}