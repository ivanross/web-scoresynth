import * as React from 'react'
export { CPText }

interface Props {
    title: string,
    value: string,
    disabled?: boolean,
    updateData: (s: string) => void
}
interface State {
    value: string
}


class CPText extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            value: props.value
        }
        this.onTextChange = this.onTextChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        return {
            value: nextProps.value
        }
    }

    onTextChange(e: any) {
        this.setState({ value: e.target.value })
    }

    onSubmit(e: any) {
        e.preventDefault()
        this.props.updateData(this.state.value)
    }

    render() {
        const { disabled } = this.props
        const { value } = this.state
        return (
            <div className="cp-comp-text cp-comp">
                <form onSubmit={this.onSubmit}>
                    <div className="cp-comp-title">
                        {this.props.title}
                    </div>
                    <div className="cp-comp-input">
                        <input
                            type="text"
                            value={value}
                            disabled={disabled}
                            onChange={this.onTextChange} />
                    </div>
                    <input type="submit" hidden />
                </form>
            </div>
        )
    }
}