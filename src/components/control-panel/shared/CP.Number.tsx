import * as React from 'react'
export { CPNumber }

interface Props {
    title: string,
    value: number,
    step: number,
    disabled?: boolean,
    updateData: (r: number) => void
}
interface State {
    value: number
}

class CPNumber extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            value: props.value
        }
        this.onValueChange = this.onValueChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State): State {
        return {
            value: nextProps.value
        }
    }

    onValueChange(e: any) {
        let disp = e.target.value.trim()
        if (disp.length == 0) disp = 0
        while (disp.length > 1 && disp.charAt(0) == "0") {
            disp = disp.substr(1)
        }
        this.setState({ value: disp })
    }

    onSubmit(e: any) {
        e.preventDefault()
        this.props.updateData(this.state.value)
    }

    render() {
        const { disabled } = this.props
        const { value } = this.state
        return (
            <div className="cp-comp-number cp-comp">
                <form onSubmit={this.onSubmit}>
                    <div className="cp-comp-title">
                        {this.props.title}
                    </div>
                    <div className="cp-comp-input">
                        <input
                            type="number"
                            value={value}
                            step={this.props.step}
                            disabled={disabled}
                            onChange={this.onValueChange} />
                    </div>
                    <input type="submit" hidden />
                </form>
            </div>
        )
    }
}