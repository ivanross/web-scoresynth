import * as React from 'react'
export { CPColor }

interface Props {
    title: string,
    value: string,
    updateData: (color: string) => void
}
interface State { }


class CPColor extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onColorChange = this.onColorChange.bind(this)
    }

    onColorChange(e: any) {
        this.props.updateData(e.target.value)
    }

    render() {
        return (
            <div className="cp-comp-color cp-comp">
                <form>
                    <div className="cp-comp-title">
                        {this.props.title}
                    </div>
                    <div className="cp-comp-input">
                        <input
                            type="color"
                            value={this.props.value}
                            onChange={this.onColorChange} />
                    </div>
                </form>
            </div>
        )
    }
}