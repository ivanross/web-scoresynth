import * as React from 'react'
export { CPSelect }

interface Props {
    title: string,
    value: number,
    options: string[]
    updateData: (i: number) => void
}

interface State { }

class CPSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onOptionChange = this.onOptionChange.bind(this)
    }
    onOptionChange(e: any) {
        this.props.updateData(+e.target.value)
    }

    render() {
        const { options, value } = this.props
        return (
            <div className="cp-comp-select cp-comp">
                <form onSubmit={() => { }}>
                    <div className="cp-comp-title">
                        {this.props.title}
                    </div>
                    <div className="cp-comp-input">
                        <select
                            value={value}
                            onChange={this.onOptionChange}>
                            {options.map((o, i) => {
                                return <option key={i} value={i}>{o}</option>
                            })}
                        </select>
                    </div>
                </form>
            </div>
        )
    }
}