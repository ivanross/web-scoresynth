import * as React from 'react'
import { GraphicPetriNet } from '../../logic/graphic';
import { CPNumber, CPText } from './shared';
export { ControlPanelPetriNet }

interface Props {
    net: GraphicPetriNet
}

class ControlPanelPetriNet extends React.Component<Props, {}> {
    render() {
        const { net } = this.props
        return (
            <div className="cp-content">

                <div className="cp-block">
                    <div className="cp-header">Petri Net</div>
                </div>

                <div className="cp-block">
                    <div className="cp-title">Elements</div>
                    <CPNumber
                        title={"Arcs"}
                        value={net.petriNet.arcs.length}
                        step={1}
                        disabled={true}
                        updateData={null} />
                    <CPNumber
                        title={"Places"}
                        value={net.petriNet.places.length}
                        step={1}
                        disabled={true}
                        updateData={null} />
                    <CPNumber
                        title={"Transitions"}
                        value={net.petriNet.transitions.length}
                        step={1}
                        disabled={true}
                        updateData={null} />

                    <CPText
                        title="Input Node"
                        value={net.petriNet.inNode ? net.petriNet.inNode.name : "-"}
                        disabled={true}
                        updateData={null} />

                    <CPText
                        title="Output Node"
                        value={net.petriNet.outNode ? net.petriNet.outNode.name : "-"}
                        disabled={true}
                        updateData={null} />

                </div>


            </div>
        )
    }
}