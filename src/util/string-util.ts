/**
 * Returns `true` if `str` ends with one of `searches`, 
 * `false` otherwise
 */
export function endsWith(str: string, ...searches: string[]): boolean {
    for (let s of searches) if (endsWithOne(str, s)) return true
    return false
}

function endsWithOne(str: string, search: string): boolean {
    return str.substring(str.length - search.length) === search
}

/**
 * Returns the file name from a path. 
 * 
 * `path/to/file` => `file`
 */
export function getFileNameFromString(str: string): string {
    let i = str.lastIndexOf('/')
    if (i < 0) return str
    return str.substr(++i)
}

export function validPnmlFileName(str: string): string {
    if (endsWithOne(str,'.pnml')) {
        return str.substring(0,str.indexOf('.pnml'))
    } 
    return str    
}

export function validXmlFileName(str: string): string {
    if (endsWithOne(str,'.xml')) {
        return str.substring(0,str.indexOf('.xml'))
    } 
    return str    
}