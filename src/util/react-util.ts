import * as React from 'react'
import * as ReactDOM from 'react-dom'

export class QuickPortal extends React.PureComponent<{ query: string }> {
    render() {
        return ReactDOM.createPortal(
            this.props.children,
            document.querySelector(this.props.query)
        )
    }
}